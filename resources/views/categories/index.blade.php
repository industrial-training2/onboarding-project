@extends('layouts.app')

@section('main-content')

    <div class="container">
        <div class="m-4">
            <div class="d-flex justify-content-between">
                <div class="mb-4">
                    <h1 class="mb-0 text-gray-800">Categories</h1>
                </div>
                <div class="mb-4 m-2">
                    <a href="{{route("categories.create")}}" class="btn btn-primary">Create Category</a>
                </div>
            </div>
            <div class="mb-4">
                <div class="d-flex justify-content-end">

                    <button  id="export" class="btn btn-secondary m-2">Export Excel</button>

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-info m-2" data-toggle="modal" data-target="#exampleModal">
                        Import Categories
                    </button>
                </div>
            </div>
            <div class="card shadow md-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <form action="{{route("categories.index")}}" method="GET" id="searchForm">
                            <div class="accordion mb-4">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="heading-1">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-1" aria-expanded="false">
                                            Advance Filter Options
                                        </button>
                                    </h2>
                                    <div id="collapse-1" class="accordion-collapse collapse" data-bs-parent="#accordion-example" style="">
                                        <div class="accordion-body pt-0">
                                            <div class="row mb-4">

                                                <div class="d-flex justify-content-between m-2">
                                                    <div class="form-group col-md-3 m-2">
                                                        <label class="form-label">Search Name</label>
                                                        <input type="search" id="name" class="form-control" name="name">
                                                    </div>
                                                    <div class="form-group col-md-3 m-2">
                                                        <label class="form-label">Search Description</label>
                                                        <input type="search" id="description" class="form-control" name="description">
                                                    </div>
                                                    <div class="form-group col-md-3 m-2">
                                                        <label class="form-label">Search Status</label>
                                                        <select class="form-control" id="status" name="status">
                                                            <option value="" id="null" disabled selected></option>
                                                            <option value="1">Active</option>
                                                            <option value="0">InActive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-start m-2">
                                                    <div class="form-group col-md-3 me-8 m-2">
                                                        <label class="form-label">Start Date</label>
                                                        <input type="date" id="start-date" class="form-control" name="start_date">
                                                    </div>
                                                    <div class="form-group col-md-3 m-2">
                                                        <label class="form-label">End Date</label>
                                                        <input type="date" id="end-date" class="form-control" name="end_date">
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end">
                                                    <button type="button" id="clear" class="btn rounded-pill me-2">Clear</button>
                                                    <button type="submit" id="submit" class="btn btn-success">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                            {{ $dataTable->table() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Categories</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex justify-content-center"><a href="{{route("categories.exportSampleSheet")}}" id="export" class="btn btn-warning m-4 align-center">Export Sample Excel</a></div>
                    <form action="{{ route("categories.import") }}" enctype="multipart/form-data" method="POST" class="d-flex justify-content-between m-4">
                        @method("POST")
                        @csrf
                        <input type="file" name="file">
                        <button type="submit" class="btn btn-info">Import Excel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("styles")
    <link href={{ asset("vendor/datatables/dataTables.bootstrap4.min.css") }} rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section("scripts")
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <script src={{ asset("vendor/datatables/jquery.dataTables.min.js") }}></script>
    <script src= {{ asset("vendor/datatables/dataTables.bootstrap4.min.js") }}></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        let table=$("#categories-table").DataTable({"serverSide":true,"dom":"lrtrp","processing":true,"ajax":{"url":"http:\/\/localhost:8000\/categories","type":"GET","data":function(data) {
                    for (var i = 0, len = data.columns.length; i < len; i++) {
                        if (!data.columns[i].search.value) delete data.columns[i].search;
                        if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                        if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                        if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                    }
                    data.name = $("#name")[0].value;
                    data.description = $("#description")[0].value;
                    data.status = $("#status")[0].value;
                    data.startDate = $("#start-date")[0].value;
                    data.endDate = $("#end-date")[0].value;
                    delete data.search.regex;}},"columns":[{"data":"id","name":"id","title":"Id","orderable":true,"searchable":true},{"data":"name","name":"name","title":"Name","orderable":true,"searchable":true},{"data":"description","name":"description","title":"Description","orderable":true,"searchable":true},{"data":"is_active","name":"is_active","title":"status","orderable":true,"searchable":true},{"data":"created_at","name":"created_at","title":"Created at","orderable":true,"searchable":true},{"data":"updated_at","name":"updated_at","title":"Updated at","orderable":true,"searchable":true},{"data":"Actions","name":"Actions","title":"Actions","orderable":true,"searchable":true}],"order":[0,"desc"],"select":{"style":"single"},"lengthMenu":[[10,20,30,40],["10 rows","20 rows","30 rows","50 rows","100 rows"]],"scrollY":false});

        class MyURL {
            static currentURL;
        }

        $("#start-date").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d",
        });
        $("#end-date").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d",
        });
        $("#clear").on("click", (evt) => {
           $("#name")[0].value = "";
           $("#description")[0].value = "";
           $("#status").val('').trigger("change");
            $("#start-date")[0].value = "";
            $("#end-date")[0].value = "";
           table.ajax.reload();
           MyURL.currentURL = window.location.toString();
        });

        $("#status").select2({
            placeholder: "Status",
            allowClear: true,
            width: 'element',

        });
        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        })

        MyURL.currentURL = window.location.toString();

        $("#searchForm").on("submit", function(evt) {
            evt.preventDefault();
            let current_URL = new URL(window.location);
            let new_URL = new URLSearchParams();
            let name = this.elements["name"].value;
            let description = this.elements["description"].value;
            let status = this.elements["status"].value;
            let startDate = $("#start-date")[0].value;
            let endDate = $("#end-date")[0].value;
            if(name!=="") {
                new_URL.append("name", name)
            }
            if(description!=="") {
                new_URL.append("description", description)
            }
            if(status!=="") {
                new_URL.append("status", status);
            }
            if(startDate !== "") {
                new_URL.append("startDate", startDate)
            }
            if(endDate !== "") {
                new_URL.append("endDate", endDate)
            }
            table.ajax.reload();
            let str = (current_URL.toString() + "?" + new_URL.toString());
            MyURL.currentURL = str;
            console.log(MyURL.currentURL);
        });

        $("#export").click(function (evt) {
            evt.preventDefault();
            let current_url = (new URL(MyURL.currentURL.toString())).searchParams;
            let params = current_url.toString();
            let new_URL = new URL("{{route("categories.export")}}");
            window.location = (new_URL.href + "?" + params);
        });
    </script>

@endsection
