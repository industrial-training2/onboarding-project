<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ asset("css/tabler.min.css")}}" rel="stylesheet"/>
    <link href="{{ asset("css/tabler-flags.min.css")}}" rel="stylesheet"/>
    <link href="{{ asset("css/tabler-payments.min.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/tabler-vendors.min.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/demo.min.css") }}" rel="stylesheet"/>
    <link href="{{ asset("css/materialize.min.css") }}" rel="stylesheet"/>
</head>
<body>
<div class="row">
    <div class="col-md-12">
        <h1>Order Cancellation Successful</h1>
    </div>
    <div class="col-md-12">
        <div class="row">
            <p>Dear, {{ $order->customer_name }}</p>
            <p>Your Order has been successfully cancelled. Here are the details:</p>
        </div>
    </div>
    <div class="col-md-12">
        <p>Order Number: {{ $order->id }}</p>
        <p>Order Date: {{ $order->order_date }}</p>
        <p>Total: {{ $order->net_total }}</p>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <p>If you have any questions, feel free to reply to this email or contact our support team.</p>
            </div>
        </div>
    </div>
    <div class="footer">
        <p>Thank You for shopping with us!</p>
    </div>
</div>
</body>
