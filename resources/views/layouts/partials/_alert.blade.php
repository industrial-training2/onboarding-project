@if (session("success"))
    <div class="alert alert-success text-white bg-success">
        {{ session("success") }}
    </div>
@endif

@if (session("error"))
    <div class="alert alert-danger text-white bg-danger">
        {{ session("error") }}
    </div>
@endif
