@extends('layouts.app')
@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-center">
                <div class="col-md-4 mt-6">
                        <div class="card p-5 border rounded-4">
                            <h1 class="align-center">
                                Create Product
                            </h1>
                            <form action="{{ route("products.store")}}" method="POST" id="createProductForm">
                                @csrf
                                @method('POST')
                            <div class="form-row">
                                <div id="productForm">
                                    <div id="innerProduct-no" class="inner-prod-no">
                                        <div class="mb-3 mt-3">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">Category</div>
                                                    <select class="form-select category" id="category" name="products[0][category_id]">
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endforeach
{{--                                                        <option value="1"></option>--}}
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mb-3">
                                                        <label class="form-label">Name</label>
                                                        <input type="text" value="{{old("products.*.name")}}" class="form-control name" name="products[0][name]" placeholder="Name of Product"/>
                                                        @error("products.*.name")
                                                        <span class="text-danger">{{$errors->first("products.*.name")}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Description</label>
                                            <textarea class="form-control description" name="products[0][description]" rows="5" style="resize: none" placeholder="Some short description of product">{{old("products.*.description")}}</textarea>
                                            <span class="text-danger">{{$errors->first("products.*.description")}}</span>

                                        </div>

                                        <div class="mb-3">
                                            <div class="row">
                                                <div class=" col-md-6">
                                                    <label class="form-label">Price</label>
                                                    <input type="text" value="{{old("products.*.price")}}" class="form-control price" step="0.01" name="products[0][price]" placeholder="₹0.00"/>
                                                    <span class="text-danger">{{$errors->first("products.*.price")}}</span>
                                                </div>
                                                <div class=" col-md-6">
                                                    <label class="form-label">Stock</label>
                                                    <input type="text" value="{{old("products.*.stock")}}" class="form-control stock" name="products[0][stock]" placeholder="0"  />
                                                    <span class="text-danger">{{$errors->first("products.*.stock")}}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="form-label">Status</div>
                                            <select class="form-select" id="status" name="products[0][status]">
                                                <option value="1">Active</option>
                                                <option value="2">InActive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {{--                                Repeatative html--}}
                                {{--                                End--}}
                                <div class="mb-3">
                                    <button type="button" class="btn" id="add"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4"/>
                                        </svg> Add more</button>
                                    <button type="button" class="btn btn-outline-danger ms-3 d-none" id="deleteProduct">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash ms-0 me-1" viewBox="0 0 16 16">
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"/>
                                            <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"/>
                                        </svg>  Delete
                                    </button>
                                </div>
                                <div class="mb-3 card-footer p-0 pt-3">
                                    <input id="submit" class="btn btn-primary" type="submit" value="Submit">

                                </div>
                            </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("styles")
    <style>
        .error {
            color: red;
        }
    </style>
@endsection

@section("scripts")
    <script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset("vendor/jquery-validation/dist/jquery.validate.js")}}"></script>
    <script src="{{asset("vendor/jquery-validation/dist/jquery.validate.min.js")}}"></script>
    <script src="{{asset("js/products/create.js")}}"></script>
@endsection

