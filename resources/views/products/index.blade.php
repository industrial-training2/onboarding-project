@extends('layouts.app')
@section('main-content')
    <div class="container">
        <div class="m-4">
            <div class="d-flex justify-content-between">
                <div class="mb-4">
                    <h1 class="mb-0 text-gray-800">Products</h1>
                </div>
                <div class="mb-4 m-2">
                    <a href="{{route("products.create")}}" class="btn btn-primary">Create Product</a>
                </div>
            </div>
            <div class="mb-4">
                <div class="d-flex justify-content-end">

                    <button  id="export" class="btn btn-secondary m-2">Export Excel</button>

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-info m-2" data-toggle="modal" data-target="#exampleModal">
                        Import Products
                    </button>
                </div>
            </div>
            <div class="card shadow md-4">
                <div class="card-body">
                    <div class="table-responsive">
                            <form action="{{route("products.index")}}" method="GET" id="searchForm">
                                <div class="accordion mb-4">
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="heading-1">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-1" aria-expanded="false">
                                                Advance Filter Options
                                            </button>
                                        </h2>
                                        <div id="collapse-1" class="accordion-collapse collapse" data-bs-parent="#accordion-example" style="">
                                            <div class="accordion-body pt-0">
                                                <div class="row">

                                                    <div class="d-flex justify-content-between m-2">
                                                        <div class="form-group col-md-3 m-2">
                                                            <label class="form-label">Search Name</label>
                                                            <input type="search" id="name" class="form-control" name="name" placeholder="Enter Name">
                                                        </div>
                                                        <div class="form-group col-md-3 m-2">
                                                            <label class="form-label">Search Description</label>
                                                            <input type="search" id="description" class="form-control" name="description" placeholder="Enter Description">
                                                        </div>
                                                        <div class="form-group col-md-3 m-2">
                                                            <label class="form-label">Search Price</label>
                                                            <input type="search" id="price" class="form-control" name="price" placeholder="Enter Price">
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between m-2 mb-3">
                                                        <div class="form-group col-md-3 m-2">
                                                            <label class="form-label">Search Stock</label>
                                                            <input type="search" id="stock" class="form-control" name="stock" placeholder="Enter Stock">
                                                        </div>

                                                        <div class="form-group col-md-3 m-2">
                                                            <label class="form-label">Search Category</label>
                                                            <select id="category" class="form-control select2" name="category">
                                                                <option value="" disabled selected></option>
                                                                @foreach($categories as $category)
                                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="form-group col-md-3 m-2">
                                                            <label>Search Status</label>
                                                            <select class="form-control select2" id="status" name="status">
                                                                <option value="" disabled selected></option>
                                                                <option value="1">Active</option>
                                                                <option value="0">InActive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-start m-2">
                                                        <div class="form-group col-md-3 me-8 m-2">
                                                            <label class="form-label">Start Date</label>
                                                            <input type="date" id="start-date" class="form-control" name="start_date">
                                                        </div>
                                                        <div class="form-group col-md-3 m-2">
                                                            <label class="form-label">End Date</label>
                                                            <input type="date" id="end-date" class="form-control" name="end_date">
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-end">
                                                        <button type="button" id="clear" class="btn rounded-pill me-2">Clear</button>
                                                        <button type="submit" id="submit" class="btn btn-success">Search</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        {{ $dataTable->table() }}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Products</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex justify-content-center"><a href="{{route("products.exportSampleSheet")}}" id="export" class="btn btn-warning m-4 align-center">Export Sample Excel</a></div>
                    <form action="{{ route("products.import") }}" enctype="multipart/form-data" method="POST" class="d-flex justify-content-between m-4" id="import-form">
                        @method("POST")
                        @csrf
                        <input type="file" name="file" class="file">
                        <button type="submit" class="btn btn-info">Import Excel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("styles")
    {{--    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">--}}
    <link href={{ asset("vendor/datatables/dataTables.bootstrap4.min.css") }} rel="stylesheet">
{{--    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.css" />--}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section("scripts")
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <script src={{ asset("vendor/datatables/jquery.dataTables.min.js") }}></script>
    <script src= {{ asset("vendor/datatables/dataTables.bootstrap4.min.js") }}></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="{{asset("vendor/jquery-validation/dist/jquery.validate.min.js")}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
    <script>
        let table = $("#products-table").DataTable({"serverSide":true,"dom":"lrtrp","processing":true,"ajax":{"url":"http:\/\/localhost:8000\/products","type":"GET","data":function(data) {
        for (var i = 0, len = data.columns.length; i < len; i++) {
            if (!data.columns[i].search.value) delete data.columns[i].search;
            if (data.columns[i].searchable === true) delete data.columns[i].searchable;
            if (data.columns[i].orderable === true) delete data.columns[i].orderable;
            if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
            data.name = $("#name")[0].value;
            data.description = $("#description")[0].value;
            data.stock = $("#stock")[0].value;
            data.price = $("#price")[0].value;
            data.status = $("#status")[0].value;
            data.category = $("#category")[0].value;
            data.startDate = $("#start-date")[0].value;
            data.endDate = $("#end-date")[0].value;
        }
        delete data.search.regex;}},"columns":[{"data":"id","name":"id","title":"Id","orderable":true,"searchable":true},{"data":"name","name":"name","title":"Name","orderable":true,"searchable":true},{"data":"description","name":"description","title":"Description","orderable":true,"searchable":true},{"data":"price","name":"price","title":"Price","orderable":true,"searchable":true},{"data":"stock","name":"stock","title":"Stock","orderable":true,"searchable":true},{"data":"category_id","name":"category_id","title":"category","orderable":true,"searchable":true},{"data":"is_active","name":"is_active","title":"status","orderable":true,"searchable":true},{"data":"created_at","name":"created_at","title":"Created at","orderable":true,"searchable":true},{"data":"updated_at","name":"updated_at","title":"Updated at","orderable":true,"searchable":true},{"data":"Actions","name":"Actions","title":"Actions","orderable":true,"searchable":true}],"order":[0,"desc"],"select":{"style":"single"},"lengthMenu":[[10,20,30,40],["10 rows","20 rows","30 rows","50 rows","100 rows"]],"scrollY":false});
        class MyURL {
            static currentURL;
        }
        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        })
        $(".select2").select2();

        $("#start-date").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d",
        });
        $("#end-date").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d",
        });
        $("#clear").on("click", (evt) => {
            $("#name")[0].value = "";
            $("#description")[0].value = "";
            $("#price")[0].value = "";
            $("#stock")[0].value = "";
            $("#category").val('').trigger("change");
            $("#status").val('').trigger("change");
            $("#start-date")[0].value = "";
            $("#end-date")[0].value = "";
            table.ajax.reload();
            MyURL.currentURL = window.location.toString();
        });

        $("#import-form").validate({
            rules: {
                file: {
                    required: true,
                    extension: 'csv|xls'
                }
            }
        });
        console.log($("#import-form"));
        MyURL.currentURL = window.location.toString();

        $("#searchForm").on("submit", function(evt) {
            console.log("Hello");
            evt.preventDefault();
            let current_URL = new URL(window.location);
            let new_URL = new URLSearchParams();
            let name = this.elements["name"].value;
            let description = this.elements["description"].value;
            let price = this.elements["price"].value;
            let stock = this.elements["stock"].value;
            let category = $("#category")[0].value;
            let status = this.elements["status"].value;
            let startDate = $("#start-date")[0].value;
            let endDate = $("#end-date")[0].value;
            if(name!=="") {
                console.log(name);
                new_URL.append("name", name)
            }
            if(description!=="") {
                new_URL.append("description", description)
            }
            if(price!=="") {
                new_URL.append("price", price)
            }
            if(stock!=="") {
                new_URL.append("stock", stock);
            }
            if(category!=="") {
                new_URL.append("category", category);
                console.log(new_URL);
            }
            if(status!=="") {
                new_URL.append("status", status);
            }
            if(startDate !== "") {
                new_URL.append("startDate", startDate)
            }
            if(endDate !== "") {
                new_URL.append("endDate", endDate)
            }
            table.ajax.reload();
            let str = (current_URL.toString() + "?" + new_URL.toString());
            MyURL.currentURL = str
            console.log(MyURL.currentURL);
        });

        $("#export").click(function (evt) {
            evt.preventDefault();
            let current_url = (new URL(MyURL.currentURL.toString())).searchParams;
            let params = current_url.toString();
            let new_URL = new URL("{{route("products.export")}}");
            window.location = (new_URL.href + "?" + params);
        });
    </script>

@endsection
