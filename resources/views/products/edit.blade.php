@extends('layouts.app')
@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-center">
                <div class="col-md-4 mt-6">
                    <div class="card p-5 border rounded-4">
                        <h1 class="align-center">
                            Edit Product
                        </h1>
                        <form action="{{ route("products.update", $product)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-row">
                                <div id="productForm">
                                    <div id="innerProduct" class="inner">
                                        <div class="mb-3 mt-3">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-label">Category</div>
                                                    <select class="form-select" id="category" name="category_id">
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}" {{$product->category_id === $category->id ? 'selected' : ''}}>{{$category->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mb-3">
                                                        <label class="form-label">Name</label>
                                                        <input type="text" class="form-control" name="name" value="{{$product->name}}" placeholder="Name of Product" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Description</label>
                                            <textarea class="form-control" name="description" rows="5" style="resize: none" placeholder="Some short description of product">{{$product->description}}</textarea>
                                        </div>

                                        <div class="mb-3">
                                            <div class="row">
                                                <div class=" col-md-6">
                                                    <label class="form-label">Price</label>
                                                    <input type="number" class="form-control" name="price" value="{{$product->price}}" placeholder="₹0.00" />
                                                </div>
                                                <div class=" col-md-6">
                                                    <label class="form-label">Stock</label>
                                                    <input type="number" class="form-control" name="stock" value="{{$product->stock}}" placeholder="0" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <div class="form-label">Status</div>
                                            <select class="form-select" id="status" name="status">
                                                <option value="1" {{ $product->is_active ? 'selected' : '' }}>Active</option>
                                                <option value="2" {{ !$product->is_active ? 'selected' : '' }}>InActive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3 card-footer p-0 pt-3">
                                    <input class="btn btn-primary" type="submit" value="Submit">

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
