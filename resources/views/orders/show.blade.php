@extends('layouts.app')
@section('main-content')
    <div class="container">
        <div class="m-4">
            <div class="card shadow md-4">
                <div class="card-body">
                    <div class="table-responsive">
                        {{ $dataTable->table() }}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section("styles")
    {{--    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">--}}
    <link href={{ asset("vendor/datatables/dataTables.bootstrap4.min.css") }} rel="stylesheet">
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.css" />--}}
@endsection

@section("scripts")
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <script src={{ asset("vendor/datatables/jquery.dataTables.min.js") }}></script>
    <script src= {{ asset("vendor/datatables/dataTables.bootstrap4.min.js") }}></script>
    {{ $dataTable->scripts() }}
@endsection
