@extends('layouts.app')
@section('main-content')
    <div class="container">
        <div class="m-4">
            <div class="d-flex justify-content-between">
                <div class="mb-4">
                    <h1 class="mb-0 text-gray-800">Orders</h1>
                </div>
                <div class="mb-4 m-2">
                    <a href="{{route("orders.create")}}" class="btn btn-primary">Place Order</a>
                </div>
            </div>
            <div class="mb-4">
                <div class="d-flex justify-content-end">

                    <button  id="export" class="btn btn-secondary m-2">Export Excel</button>

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-info m-2" data-toggle="modal" data-target="#exampleModal">
                        Import Orders
                    </button>
                </div>
            </div>
            <div class="card shadow md-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <form action="{{route("orders.index")}}" method="GET" id="searchForm">
                            <div class="accordion mb-4">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="heading-1">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-1" aria-expanded="false">
                                            Advance Filter Options
                                        </button>
                                    </h2>
                                    <div id="collapse-1" class="accordion-collapse collapse" data-bs-parent="#accordion-example" style="">
                                        <div class="accordion-body pt-0">
                                            <div class="row">
                                                <div class="d-flex justify-content-between">
                                                    <div class="form-group col-md-3 m-2">
                                                        <label class="form-label p-2">Search By Name</label>
                                                        <input type="search" id="name" class="form-control form-control-sm p-2" name="name" placeholder="Name">
                                                    </div>
                                                    <div class="form-group col-md-3 m-2">
                                                        <label class="form-label p-2">Search By Email</label>
                                                        <input type="text" id="email" class="form-control form-control-sm p-2" name="email" placeholder="Email">
                                                    </div>
                                                    <div class="form-group col-md-3 m-2">
                                                        <label class="form-label p-2">Search By OrderDate</label>
                                                        <input type="date" id="order-date" class="form-control form-control-sm date p-2" name="orderDate" placeholder="Order Date">
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <div class="col-md-3 m-2">
                                                        <label class="form-label">Search By Product</label>
                                                        <select class="select2-product product form-control" name="product_id" id="product">
                                                            <option value=""></option>
                                                            @foreach($products as $product)
                                                                <option value="{{$product->id}}">{{$product->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 m-2">
                                                        <label class="form-label">Start Date</label>
                                                        <input type="date" id="start-date" class="form-control date" name="start_date">
                                                    </div>
                                                    <div class="col-md-3 m-2">
                                                        <label class="form-label">End Date</label>
                                                        <input type="date" id="end-date" class="form-control date" name="end_date">
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end mt-2">
                                                    <button type="button" id="clear" class="btn rounded-pill me-2">Clear</button>
                                                    <button type="submit" id="submit" class="btn btn-success">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="container">
                            <div class="col-md-12">
                                {{ $dataTable->table() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex justify-content-center"><a href="{{route("orders.exportSampleOrder")}}" id="export" class="btn btn-warning m-4 align-center">Export Sample Excel</a></div>
                    <form action="{{ route("orders.import") }}" enctype="multipart/form-data" method="POST" class="d-flex justify-content-between m-4" id="importData">
                        @method("POST")
                        @csrf
                        <div class="col-md-12">
                            <div class="d-flex justify-content-between flex-wrap gap-2">
                                <div class="col-md-4"><label for="name" class="form-label name">Enter Name:</label><input type="text" name="name" class="form-control-sm name"></div>
                                <div class="col-md-4"><label for="email" class="form-label email">Enter Email:</label><input type="text" name="email" class="form-control-sm email"></div>
                                <div class="col-md-4"><label for="date" class="form-label">Order Date:</label><input type="date" name="date" class="form-control-sm date"></div>
                                <div class="mt-3">
                                    <input type="file" name="file" class="file">
                                    <button type="submit" class="btn btn-info">Import Excel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("styles")
    {{--    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">--}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href={{ asset("vendor/datatables/dataTables.bootstrap4.min.css") }} rel="stylesheet">
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.css" />--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section("scripts")
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <script src={{ asset("vendor/datatables/jquery.dataTables.min.js") }}></script>
    <script src= {{ asset("vendor/datatables/dataTables.bootstrap4.min.js") }}></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="{{asset("vendor/jquery-validation/dist/jquery.validate.min.js")}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        let table = $("#orders-table").DataTable({"serverSide":true,"dom":"lrtrp","processing":true,"ajax":{"url":"http:\/\/localhost:8000\/orders","type":"GET","data":function(data) {
                    for (var i = 0, len = data.columns.length; i < len; i++) {
                        if (!data.columns[i].search.value) delete data.columns[i].search;
                        if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                        if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                        if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                    }
                    data.name = $("#name")[0].value;
                    data.email = $("#email")[0].value;
                    data.order_date = $("#order-date")[0].value;
                    data.product_id = $(".select2-product")[0].value;
                    data.startDate = $("#start-date")[0].value;
                    data.endDate = $("#end-date")[0].value;
                    delete data.search.regex;}},"columns":[{"data":"id","name":"id","title":"Id","orderable":true,"searchable":true},{"data":"customer_name","name":"customer_name","title":"Customer Name","orderable":true,"searchable":true},{"data":"customer_email","name":"customer_email","title":"Customer Email","orderable":true,"searchable":true},{"data":"order_date","name":"order_date","title":"Order Date","orderable":true,"searchable":true},{"data":"net_total","name":"net_total","title":"Net Total","orderable":true,"searchable":true},{"data":"order_status","name":"order_status","title":"Order Status","orderable":true,"searchable":true},{"data":"created_at","name":"created_at","title":"Created At","orderable":true,"searchable":true},{"data":"updated_at","name":"updated_at","title":"Updated At","orderable":true,"searchable":true},{"data":"Actions","name":"Actions","title":"Actions","orderable":true,"searchable":true}],"order":[0,"desc"],"select":{"style":"single"},"lengthMenu":[[10,20,30,40],["10 rows","20 rows","30 rows","50 rows","100 rows"]],"scrollY":false});
        class MyURL {
            static currentURL;
        }

        $("#clear").on("click", (evt) => {
            $("#name")[0].value = "";
            $("#email")[0].value = "";
            $("#order-date")[0].value = "";
            $(".select2-product").val('').trigger("change");
            $("#start-date")[0].value = "";
            $("#end-date")[0].value = "";
            table.ajax.reload();
            MyURL.currentURL = window.location.toString();
        });

        jQuery.validator.addClassRules("name", {
            required: true,
            minlength: 2,
            maxlength: 255
        });

        jQuery.validator.addClassRules("date", {
            required: true,
        });

        jQuery.validator.addClassRules("email", {
            required: true,
            email: true,
        });

        jQuery.validator.addClassRules("file", {
            required: true,
            file: true,
        });

        $("#importData").validate();

        MyURL.currentURL = window.location.toString();
        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        })
        $(document).ready(function () {
            $(".date").flatpickr();
        })

        $(".date").flatpickr();

        $("#searchForm").on("submit", function (evt) {
            evt.preventDefault();
            let current_URL = new URL(window.location);
            let new_URL = new URLSearchParams();
            let name = this.elements["name"].value;
            let email = this.elements["email"].value;
            let orderDate = this.elements["orderDate"].value;
            let product = $(".select2-product")[0].value;
            let startDate = $("#start-date")[0].value;
            let endDate = $("#end-date")[0].value;
            if (name !== "") {
                new_URL.append("name", name);
            }
            if (email !== "") {
                new_URL.append("email", email);
            }
            if (orderDate !== "") {
                new_URL.append("orderDate", orderDate);
            }
            if(product !== "") {
                new_URL.append("product", product);
            }
            if(startDate !== "") {
                new_URL.append("startDate", startDate);
            }
            if(endDate !== "") {
                new_URL.append("endDate", endDate);
            }
            table.ajax.reload();
            let str = (current_URL.toString() + "?" + new_URL.toString());
            MyURL.currentURL = str;
            console.log(MyURL.currentURL);
        });


        $("#export").click(function (evt) {
            evt.preventDefault();
            console.log("hello");
            let current_url = (new URL(MyURL.currentURL.toString())).searchParams;
            let params = current_url.toString();
            let new_URL = new URL("{{route("orders.export")}}");
            window.location = (new_URL.href + "?" + params);
        });
        $(".select2-product").select2({
            placeholder: "Search By Product",
            allowClear: true,
            width: "100%"
        });
    </script>
@endsection
