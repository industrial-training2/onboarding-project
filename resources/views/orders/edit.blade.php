@extends('layouts.app')

@section('main-content')
    <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <div class="col-md-4 mt-6">
                <form action="{{route("orders.update", $order)}}" method="POST" class="repeater" id="orderForm">
                    @csrf
                    @method('PUT')
                    <div class="card p-5">
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label class="form-label">Enter your Name</label>
                                <input type="text" class="form-control name" name="name" value="{{old("name", $order->customer_name)}}" placeholder="Enter Name"/>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Order Date</label>
                                <input type="date" class="form-control date" name="date" value="{{old("date", $order->order_date)}}" placeholder="Enter Date"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <label class="form-label">Enter your Email</label>
                                <input type="email" class="form-control email" name="email" id="email" value="{{old("email", $order->customer_email)}}" placeholder="abc@xyz.com"/>
                            </div>
                        </div>
                        <div data-repeater-list="orders">
                            @if(old("orders"))
                                @foreach(old("orders") as $i => $order_item)
                                    <div data-repeater-item>
                                        <h1 class="align-center">Edit Order</h1>
                                        <div class="form-row">
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <label class="form-label">Select Category</label>
                                                    <select class="select2-category form-control category" name="category">
                                                        <option value=""></option>
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}" {{$order_item["category"] == $category->id ? 'selected' : ''}}>{{$category->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error("orders.".$i.".category") <span class="text-danger">{{ $message }} @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-label">Select Product</label>
                                                    <select class="select2-product product form-control" name="product_id">
                                                        <option value=""></option>
                                                    </select>
                                                    @error('orders.'.$i.'..product_id')<span class="text-danger">{{ $message }}</span> @enderror
                                                    <span class="no-active-products d-none">No Active Products</span>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <label class="form-label">Quantity</label>
                                                    <input type="number" data-previous_quantity="{{count($order->order_items) < [$i] ? 0 : $order->$order_items[$i]->quantity}}" class="form-control quantity" step="1" name="quantity" placeholder="0" value="{{$order_item["quantity"]}}"/>
                                                    <span class="text-danger quantity-error"></span>
                                                    @error('orders.'.$i.'.quantity')<span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-label">Unit Price</label>
                                                    <input type="text" class="form-control unit-price" readonly name="unit_price" placeholder="₹0" value="{{$order_item["unit_price"]}}" />
                                                    @error('orders.'.$i.'.unit_price')<span class="text-danger">{{ $message }}</span> @enderror
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <label class="form-label">Total</label>
                                                    <input type="text" readonly class="form-control total-amount" value="{{$order_item["quantity"] * $order_item["unit_price"]}}" name="total_price" placeholder="₹0"  />
                                                </div>
                                            </div>
                                            <div class="card-footer p-2"></div>
                                        </div>
                                        <div class="mb-2">
                                            <button data-repeater-delete type="button" class="btn btn-outline-danger" id="deleteProduct" >Delete</button>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                @foreach($order->order_items as $i => $order_item)
                                    <div data-repeater-item>
                                        <input type="hidden" name="order_item_id" value="{{$order_item->id}}">
                                        <h1 class="align-center">Edit Order</h1>
                                        <div class="form-row">
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <label class="form-label">Select Category</label>
                                                    <select class="select2-category category form-control" name="category">
                                                        <option value=""></option>
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}" {{$order_item->product->category_id === $category->id ? 'selected' : '' }}>{{$category->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-label">Select Product</label>
                                                    <select class="select2-product product form-control" name="product_id">
                                                        @foreach($order_item->product->category->products as $product)
                                                            <option value="{{$product->id}}" {{$product->id === $order_item->product->id ? 'selected': ''}} data-quantity="{{$product->stock}}">{{$product->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="no-active-products d-none">No Active Products</span>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <label class="form-label">Quantity</label>
                                                    <input type="number" data-previous_quantity="{{$order_item->quantity}}" class="form-control quantity" step="1" name="quantity" placeholder="0" value="{{$order_item->quantity}}"/>
                                                    <span class="text-danger">{{$errors->first("products.*.price")}}</span>
                                                    <span class="text-danger quantity-error"></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-label">Unit Price</label>
                                                    <input type="text" class="form-control unit-price" readonly name="unit_price" placeholder="₹0" value="{{$order_item->unit_price}}" />
                                                    <span class="text-danger">{{$errors->first("products.*.stock")}}</span>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <div class="col-md-6">
                                                    <label class="form-label">Total</label>
                                                    <input type="text" readonly class="form-control total-amount" value="{{$order_item->quantity * $order_item->unit_price}}" name="total_price" placeholder="₹0"  />
                                                </div>
                                            </div>
                                            <div class="card-footer p-2"></div>
                                        </div>
                                        <div class="mb-2">
                                            <button data-repeater-delete type="button" class="btn btn-outline-danger" id="deleteProduct" >Delete</button>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="d-inline d-flex justify-content-start float-left"><input data-repeater-create type="button" class="btn mb-3" id="add" value="Add more"/></div>

                        <div class=""><label class="form-label grand-amount">Total Amount: {{$order->net_total}}</label> </div>
                        <input type="hidden" class="input-net_total" value="{{$order->net_total}}" name="net_total"/>
                        <div class="mb-3 card-footer p-0 pt-3 hello">
                            <input class="btn btn-primary" type="submit" value="Submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("styles")
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section("scripts")

    <script src="{{ asset('vendor/jquery/jquery.js')}}"></script>

    <script src="{{asset("vendor/jquery-validation/dist/jquery.validate.js")}}"></script>
    <script src="{{asset("vendor/jquery-validation/dist/jquery.validate.min.js")}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset("vendor/jquery/jquery.repeater.js")}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        class TotalAmount {
            static amount = 0;
        }
        $('.date').flatpickr();
        @if(old("orders"))
        let categories = $(".select2-category");
        let oldProductId;
        @foreach(old("orders") as $i => $order_item)
            @if($order_item["category"] != "")
                 (categories[{{$i}}])
                oldProductId = {{$order_item["product_id"]}};
                setTimeout(setOldProduct, 1000, oldProductId);
           @endif
        @endforeach
        @endif
        function setOldProduct(oldProductId) {
            let products = $('.select2-product');
            products.each(function(i) {
                $(this).find('option').each(function(j) {
                    if(this.value == oldProductId) {
                        $(products[i]).val(oldProductId);
                        $(products[i]).change();
                    }
                });
            });
        }

        jQuery.validator.addClassRules("name", {
            required: true,
            minlength: 2,
            maxlength: 255
        });

        jQuery.validator.addClassRules("date", {
            required: true,
        });

        jQuery.validator.addClassRules("email", {
            required: true,
            email: true,
        });

        jQuery.validator.addClassRules("category", {
            required: true,
        });

        jQuery.validator.addClassRules("product", {
            required: true,
        });

        jQuery.validator.addClassRules("quantity", {
            required: true,
            min:0,
        });


        // $("#orderForm").validate();

        function calculateGrandTotal() {
            singleOrder = $(".total-amount");
            let totalAmount = 0;
            for(let j = 0; j < singleOrder.length; j++) {
                if(singleOrder[j].value)
                    totalAmount += parseInt(singleOrder[j].value);
            }
            TotalAmount.amount = totalAmount;
            $(".grand-amount")[0].innerHTML = `Total Amount: ${TotalAmount.amount}`;
            $(".input-net_total")[0].value = TotalAmount.amount;
        }
        calculateGrandTotal();

        $(document).ready(function() {
            $('.select2-category').select2({
                placeholder: "Select a Category",
                allowClear: true
            });

            $('.select2-product').select2({
                placeholder: "Select a Product",
                allowClear: true
            });

            $('.select2-category').on("change", (evt) => {
                getProducts(evt.target);
            });

            $(".quantity").on("input", (evt) => {
                let parent = evt.target.parentElement;
                while(!parent.classList.contains("form-row")) {
                    parent = parent.parentElement;
                }
                let productQuantity = parseInt($(parent).find(".select2-product").find(':selected')[0].dataset.quantity);
                let quantity = parseInt(evt.target.value);
                let previousQuantity = parseInt($(parent).find(".quantity")[0].dataset.previous_quantity);
                let quantityAvailable = previousQuantity + productQuantity;
                console.log(quantityAvailable);
                if(quantityAvailable < quantity) {
                    Swal.fire({
                        title: 'Out Of Stock!',
                        text: 'The product you selected is out of stock',
                        icon: 'error',
                        confirmButtonText: 'Cool'
                    });
                }
                if(quantity > 0 && quantity > quantityAvailable) {
                    console.log($(parent).find(".quantity-error"));
                    $(parent).find(".quantity-error")[0].innerHTML = `Please enter quantity below ${quantityAvailable}`;
                } else {
                    $(parent).find(".quantity-error")[0].innerHTML = '';
                    $(parent).find(".total-amount")[0].value = ($(parent).find(".unit-price")[0].value * evt.target.value);
                    calculateGrandTotal();
                }
            });


            // Repeater
            $('.repeater').repeater({
                isFirstItemUndeletable: true,
                show: function() {
                    $(this).slideDown();
                    $('.select2-container').remove();
                    $('.select2-category').select2({
                        placeholder: "Select a Category",
                        allowClear: true
                    });
                    $('.select2-product').select2({
                        placeholder: "Select a Product",
                        allowClear: true
                    });
                    $('.select2-container').css('width','100%');
                    $('.select2-category').on("change", (evt) => {
                        getProducts(evt.target);
                    });
                    $(".quantity").on("input", (evt) => {
                        let parent = evt.target.parentElement;
                        while(!parent.classList.contains("form-row")) {
                            parent = parent.parentElement;
                        }
                        let productQuantity = parseInt($(parent).find(".select2-product").find(':selected')[0].dataset.quantity);
                        let quantity = parseInt(evt.target.value);
                        if(quantity > 0 && quantity > productQuantity) {
                            console.log($(parent).find(".quantity-error"));
                            $(parent).find(".quantity-error")[0].innerHTML = `Please enter quantity below ${productQuantity}`;
                        } else {
                            $(parent).find(".quantity-error")[0].innerHTML = '';
                            $(parent).find(".total-amount")[0].value = ($(parent).find(".unit-price")[0].value * evt.target.value);
                            calculateGrandTotal();
                        }
                    });

                },
                hide: function (deleteElement) {
                    $(this).slideUp(deleteElement);
                    setTimeout(calculateGrandTotal, 500);
                },
            });
        });

        function getProducts(element) {
            let parent = element.parentElement;
            while(!parent.classList.contains("form-row")) {
                parent = parent.parentElement;
            }
            clearProducts(parent);
            let selected_id = $(parent).find(".select2-category").val();
            if(selected_id === "") {
                return;
            }
            product_url = '{{route("orders.getProducts")}}';
            $.ajax({
                url: product_url,
                method: "POST",
                data: {
                    "category_id": parseInt(selected_id),
                },
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            }).then((response) => {
                clearProducts(parent);
                let products = JSON.parse(response);
                if(products.length === 0) {
                    $(parent).find(".no-active-products").removeClass("d-none");
                }
                for(let i = 0; i < products.length; i++) {
                    let productId = products[i].id;
                    let productName = products[i].name;
                    let productPrice = products[i].price;
                    let productQuantity = products[i].stock;
                    $(parent).find(".select2-product").append(`<option value='${productId}' data-price='${productPrice}' data-quantity='${productQuantity}'>${productName}</option>`)
                }
                $(".select2-product").on("change", (evt) => {
                    let selected_product_price = evt.target.options[evt.target.selectedIndex].dataset.price;
                    let parent = evt.target.parentElement;
                    while(!parent.classList.contains("form-row")) {
                        parent = parent.parentElement;
                    }
                    $(parent).find(".unit-price")[0].value = selected_product_price;
                });
            });
        }

        function clearProducts(parent) {
            $(parent).find(".select2-product")[0].innerHTML = `<option value=""></option>`;
            $(parent).find(".unit-price")[0].value = 0;
            TotalAmount.amount = TotalAmount.amount - parseInt($(parent).find(".total-amount")[0].value);
            $(parent).find(".quantity")[0].value = 0;
            $(parent).find(".total-amount")[0].value = 0;
            $(".grand-amount")[0].innerHTML = `Total Amount: ${TotalAmount.amount}`;
            $(parent).find(".no-active-products").addClass("d-none");
        }

        $("#orderForm").on("submit", (evt) => {
            calculateGrandTotal();
            let totalAmount = $(".total-amount");
            for(let i =0; i < totalAmount.length; i++) {
                totalAmount[i].disabled = false;
            }
            let unitPrice = $(".unit-price");
            for(let i =0; i < unitPrice.length; i++) {
                unitPrice[i].disabled = false;
            }
        });
    </script>
@endsection
