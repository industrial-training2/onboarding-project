<?php

namespace Products\Domain\Models;

use App\Features\Products\Domain\Models\Product;
use Database\Factories\CategoriesFactory;
use Database\Factories\ProductsFactory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic feature test example.
     */
    public function testPersistProductMethodMustReturnProductInstanceIfValidatedSuccessfully()
    {
        (new CategoriesFactory(1))->create();
        $data = [
            "name" => "Funny",
            "description" => "somthing about funntyy",
            "price" => 132,
            "stock" => 4,
            "is_active" => true,
            "category_id" => 1
        ];

        $product = Product::persistProduct($data);
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals("Funny", $product->getOriginal("name"));
        $this->assertDatabaseCount("products", 1);
        $this->assertDatabaseHas('products', ['id' => 1]);
    }

    public function testPersistProductMethodMustValidate()
    {
        $data = [
//            "name" => "Funny",
            "description" => "somthing about funnty",
            "is_active" => true,
        ];
        $this->expectException(ValidationException::class);
        $product = Product::persistProduct($data);
    }

    public function testUpdateProductMethodMustReturnProductInstanceIfValidatedSuccessfully()
    {
        (new CategoriesFactory(1))->create();
        $product = (new ProductsFactory(1))->create()->first();
        $updatedData  = $product->toArray();
        $updatedData["name"] = "Modi";

        $product = $product->updateProduct($updatedData);
        $this->assertInstanceOf(Product::class, $product);
        $this->assertDatabaseCount("products", 1);
        $this->assertDatabaseHas('products', ['id' => 1]);
    }

    public function testUpdateProductMethodMustValidate()
    {
        (new CategoriesFactory(1))->create();
        $product = (new ProductsFactory(1))->create()->first();
        $updatedData  = $product->toArray();
        $updatedData["name"] = "";

        $this->expectException(ValidationException::class);
        $product = $product->updateProduct($updatedData);
    }

    public function testdeleteProductMustDelete()
    {
        (new CategoriesFactory(1))->create();
        $product = (new ProductsFactory(1))->create()->first();
        $product = $product->deleteProduct();
        $this->assertDatabaseCount("products", 0);
    }
}
