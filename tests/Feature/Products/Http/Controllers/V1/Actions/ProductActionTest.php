<?php

namespace Products\Http\Controllers\V1\Actions;

use App\Features\Products\Http\Controllers\V1\Actions\ProductsAction;
use Database\Factories\CategoriesFactory;
use Database\Factories\ProductsFactory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ProductActionTest extends TestCase
{
    use DatabaseMigrations;
    public function testPersistProductMethodMustCreateMultipleProducts()
    {
        (new CategoriesFactory(1))->create();
        $data = [[
            "name" => "Funny",
            "description" => "somthing about funntyy",
            "price" => 132,
            "stock" => 4,
            "status" => 2,
            "category_id" => 1
        ],

        [
            "name" => "Not Funny",
            "description" => "This is for testing",
            "price" => 234,
            "stock" => 67,
            "status" => 1,
            "category_id" => 1
        ]];

        $products = (new ProductsAction())->persistProducts($data);

        $this->assertDatabaseCount("products", 2);
    }
}
