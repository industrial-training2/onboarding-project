<?php

namespace Categories\Http\Controllers\V1\Actions;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Categories\Http\Controllers\V1\Actions\CategoriesAction;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CategoriesActionTest extends TestCase
{
    use DatabaseMigrations;

    public function testPersistCategoryMethodMustReturnInstanceOfCategory()
    {
        $data = [
            "name" => "John",
            "description" => "",
            "is_active" => false,
        ];

        $category = (new CategoriesAction())->persistCategory($data);

        $this->instance(Category::class, $category);
    }
}
