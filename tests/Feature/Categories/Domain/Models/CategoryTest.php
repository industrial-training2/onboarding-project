<?php

namespace Categories\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use Database\Factories\CategoriesFactory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic feature test example.
     */
    public function testPersistCategoryMethodMustReturnCategoryInstanceIfValidatedSuccessfully()
    {
        $data = [
            "name" => "Funny",
            "description" => "somthing about funntyy",
            "is_active" => true,
        ];

        $category = Category::persistCategory($data);
        $this->assertInstanceOf(Category::class, $category);
        $this->assertEquals("Funny", $category->getOriginal("name"));
        $this->assertDatabaseCount("categories", 1);
        $this->assertDatabaseHas('categories', ['id' => 1]);
    }

    public function testPersistCategoryMethodMustValidate()
    {
        $data = [
//            "name" => "Funny",
            "description" => "somthing about funnty",
            "is_active" => true,
        ];
        $this->expectException(ValidationException::class);
        $category = Category::persistCategory($data);
    }

    public function testUpdateCategoryMethodMustReturnCategoryInstanceIfValidatedSuccessfully()
    {
        $category = (new CategoriesFactory(1))->create()->first();
        $updatedData  = $category->toArray();
        $updatedData["name"] = "Modi";

        $category = $category->updateCategory($updatedData);
        $this->assertInstanceOf(Category::class, $category);
        $this->assertDatabaseCount("categories", 1);
        $this->assertDatabaseHas('categories', ['id' => 1]);
    }

    public function testUpdateCategoryMethodMustValidate()
    {
        $category = (new CategoriesFactory(1))->create()->first();
        $updatedData  = $category->toArray();
        $updatedData["name"] = "";

        $this->expectException(ValidationException::class);
        $category = $category->updateCategory($updatedData);
    }

    public function testdeleteProductMustDelete()
    {
        $category = (new CategoriesFactory(1))->create()->first();
        $category = $category->deleteCategory();
        $this->assertDatabaseCount("categories", 0);
    }
}
