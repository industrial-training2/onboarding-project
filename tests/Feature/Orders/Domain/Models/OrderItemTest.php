<?php

namespace Orders\Domain\Models;

use App\Features\OrderItems\Domain\Models\OrderItem;
use App\Features\Orders\Domain\Exports\OrdersExport;
use App\Features\Orders\Domain\Models\Order;
use Database\Factories\CategoriesFactory;
use Database\Factories\OrderFactory;
use Database\Factories\OrderItemFactory;
use Database\Factories\ProductsFactory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use \Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class OrderItemTest extends TestCase
{
    use DatabaseMigrations;

    public function testPersistOrderItemMethodMustReturnOrderItemInstanceIfValidatedSuccessfully()
    {
        $orderItem = (new OrderFactory(1))->create()->first();
        $category = (new CategoriesFactory(1))->create()->first();
        $product = (new ProductsFactory(1))->create()->first();
        $data = [
            "order_id" => $orderItem->id,
            "product_id" => $product->id,
            "unit_price" => $product->price,
            "quantity" => 2,
        ];

        $orderItem = OrderItem::persistOrderItem($data);
        $this->assertInstanceOf(OrderItem::class, $orderItem);
        $this->assertEquals(2, $orderItem->getOriginal("quantity"));
        $this->assertDatabaseCount("order_items", 1);
        $this->assertDatabaseHas('order_items', ['id' => 1]);
    }

    public function testPersistOrderItemMethodMustValidate()
    {
        $order = (new OrderFactory(1))->create()->first();
        $category = (new CategoriesFactory(1))->create()->first();
        $product = (new ProductsFactory(1))->create()->first();
        $data = [
            "order_id" => 4,
            "product_id" => $product->id,
            "unit_price" => $product->price,
            "quantity" => 2,
        ];
        $this->expectException(ValidationException::class);
        $order = OrderItem::persistOrderItem($data);
    }

    public function testUpdateOrderItemMethodMustReturnOrderInstanceIfValidatedSuccessfully()
    {
        $order = (new OrderFactory(1))->create()->first();
        $category = (new CategoriesFactory(1))->create()->first();
        $product = (new ProductsFactory(1))->create()->first();
        $data = [
            "order_id" => $order->id,
            "product_id" => $product->id,
            "unit_price" => $product->price,
            "quantity" => 2,
        ];
        $orderItem = OrderItem::persistOrderItem($data);
        $updatedData  = $orderItem->toArray();
        $updatedData["quantity"] = 4;

        $orderItem = $orderItem->updateOrderItem($updatedData);
        $this->assertIsBool($orderItem, true);
        $this->assertDatabaseCount("order_items", 1);
        $this->assertDatabaseHas('order_items', ['id' => 1]);
        $this->assertDatabaseHas('order_items', ['quantity' => 4]);
    }

    public function testUpdateOrderItemMethodMustValidate()
    {
        $order = (new OrderFactory(1))->create()->first();
        $category = (new CategoriesFactory(1))->create()->first();
        $product = (new ProductsFactory(1))->create()->first();
        $data = [
            "order_id" => $order->id,
            "product_id" => $product->id,
            "unit_price" => $product->price,
            "quantity" => 2,
        ];
        $orderItem = OrderItem::persistOrderItem($data);
        $updatedData  = $orderItem->toArray();
        $updatedData["product_id"] = '';

        $this->expectException(ValidationException::class);
        $orderItem = $orderItem->updateOrderItem($updatedData);
    }

    public function test_user_can_download_Orders_export()
    {
        (new OrderFactory(1))->create();
        (new CategoriesFactory(1))->create();
        (new ProductsFactory(1))->create();
        (new OrderItemFactory(1))->create();
        Excel::fake();

        $this->get('/orders/export/export-data');

        Excel::assertDownloaded('orders.xlsx', function(OrdersExport $export) {
            return true;
        });
    }
}
