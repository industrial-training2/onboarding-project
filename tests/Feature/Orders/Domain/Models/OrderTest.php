<?php

namespace Orders\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Orders\Domain\Models\Order;
use Database\Factories\CategoriesFactory;
use Database\Factories\OrderFactory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use DatabaseMigrations;

    public function testPersistOrderMethodMustReturnOrderInstanceIfValidatedSuccessfully()
    {
        $data = [
            "customer_name" => "Funny",
            "customer_email" => "funy@gmail.com",
            "order_date" => "2004-05-09",
        ];

        $order = Order::persistOrder($data);
        $this->assertInstanceOf(Order::class, $order);
        $this->assertEquals("Funny", $order->getOriginal("customer_name"));
        $this->assertDatabaseCount("orders", 1);
        $this->assertDatabaseHas('orders', ['id' => 1]);
    }

    public function testPersistOrderMethodMustValidate()
    {
        $data = [
//            "customer_name" => "Funny",
            "customer_email" => "funy@gmail.com",
            "order_date" => "2004-05-09",
        ];
        $this->expectException(ValidationException::class);
        $order = Order::persistOrder($data);
    }

    public function testUpdateOrderMethodMustReturnOrderInstanceIfValidatedSuccessfully()
    {
        $order = (new OrderFactory(1))->create()->first();
        $updatedData  = $order->toArray();
        $updatedData["customer_name"] = "Modi";

        $order = $order->updateOrder($updatedData);
        $this->assertIsBool($order, true);
        $this->assertDatabaseCount("orders", 1);
        $this->assertDatabaseHas('orders', ['id' => 1]);
        $this->assertDatabaseHas('orders', ['customer_name' => 'Modi']);
    }

    public function testUpdateOrderMethodMustValidate()
    {
        $order = (new OrderFactory(1))->create()->first();
        $updatedData  = $order->toArray();
        $updatedData["customer_name"] = "";

        $this->expectException(ValidationException::class);
        $order = $order->updateOrder($updatedData);
    }
}
