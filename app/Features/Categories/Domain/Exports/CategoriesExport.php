<?php

namespace App\Features\Categories\Domain\Exports;

use App\Features\Categories\Domain\Models\Category;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CategoriesExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private Collection $categories;
    public function __construct(array $data = []) {
        if(empty($data)) {
            $this->categories = Category::all();
        }
        $queryBuilder = Category::query();
        if(isset($data["name"])) {
            $queryBuilder->where("name", "like", "%" . $data["name"] . "%");
        }
        if (isset($data["description"])) {
            $queryBuilder->where("description", "like", "%" . $data["description"] . "%");
        }
        if(isset($data["status"])) {
            $queryBuilder->where("is_active", $data["status"]);
        }
        if(isset($data["startDate"])) {
            $queryBuilder->where("created_at", ">=", $data["startDate"]);
        }
        if(isset($data["endDate"])) {
            $endDate = date("Y-m-d", strtotime($data["endDate"]."+1 days"));
            $queryBuilder->where("created_at", "<=", $endDate);
        }
        $this->categories = $queryBuilder->get();
    }


    public function collection()
    {
        return $this->categories;
    }


    public function headings(): array
    {
        return [
            "ID",
            "Name",
            "Description",
            "Is Active",
            'created_at',
            'updated_at',
        ];
    }

    public function map($row): array
    {
        $data = $row->toArray();
        $data["is_active"] = $row->is_active ? "1" : "0";
        $data["created_at"] = date_format($row->created_at, "Y-m-d");
        $data["updated_at"] = date_format($row->updated_at, "Y-m-d");
        return $data;
    }
}
