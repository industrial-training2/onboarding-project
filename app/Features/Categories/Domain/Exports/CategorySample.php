<?php

namespace App\Features\Categories\Domain\Exports;

use App\Features\Categories\Domain\Models\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class CategorySample implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle
{

    public function collection()
    {
        return Category::all("id", "name");
    }

    public function headings(): array
    {
        return [
            "id",
            "name",
        ];
    }

    public function title(): string
    {
        return "Categories";


    }
}
