<?php

namespace App\Features\Categories\Domain\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CategoriesFailureExport implements FromArray, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private array $data;
    private array $headings;
    private array $errorMessages;

    public function __construct(array $data, array $errorMessages) {
        $this->data = $data;
        $this->errorMessages = $errorMessages;
        $this->headings = array_keys($data[0]);
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return $this->headings;
    }

    public function styles(Worksheet $sheet)
    {
        $columns = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J");
        for($i = 0; $i < count($this->data); $i++) {
            $errors = $this->errorMessages[$i];
            for($j = 0; $j < count($this->headings()); $j++) {
                if(array_key_exists($this->headings[$j], $errors)) {
                    $column = $columns[$j].($i+2);
                    $errorMessages = "";
                    foreach($errors[$this->headings[$j]] as $message) {
                        $errorMessages .= $message."\n";
                    }
                    $sheet->getStyle($column)->applyFromArray([
                        'color' => ['rgb' => 'FF0000'],
                    ])->getActiveSheet()->getComment($column)->getText()->createTextRun($errorMessages);
                }
            }
        }
    }
}
