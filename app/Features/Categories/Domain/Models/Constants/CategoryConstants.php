<?php

namespace App\Features\Categories\Domain\Models\Constants;

interface CategoryConstants
{
    public const PERSIST_VALIDATION_RULES = [
            'name' => 'required|string|max:255',
            'description' => 'string',
            'is_active' => 'boolean',
        ];

    public const UPDATE_VALIDATION_RULES = [
        'name' => 'required|string|max:255',
        'description' => 'string',
        'is_active' => 'boolean',
    ];
}
