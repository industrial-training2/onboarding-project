<?php

namespace App\Features\Categories\Domain\Models;

use App\Features\Categories\Domain\Models\Constants\CategoryConstants;
use App\Features\Products\Domain\Models\Product;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Database\Factories\CategoriesFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class Category extends BaseModel implements CategoryConstants
{

    protected $table = 'categories';

    protected static function newFactory()
    {
        return CategoriesFactory::new();
    }

    public function scopeActive(Builder $query) {
        $query->where("is_active", true);
    }

    public static function persistCategory(array $data): self
    {
        Utils::validateOrThrow($data, self::persistRules());
        return DB::transaction(function () use ($data) {
            return Category::create($data);
        });
    }

    public function products() {
        return $this->hasMany(Product::class );
    }

    public function updateCategory(array $data): self
    {
        Utils::validateOrThrow($data, self::updateRules());
        DB::transaction(function () use ($data) {
            $this->update($data);
        });
        return $this;
    }

    public static function persistRules(): array
    {
        return self::PERSIST_VALIDATION_RULES;
    }

    public static function updateRules(): array
    {
        return self::UPDATE_VALIDATION_RULES;
    }

    public function deleteCategory(): bool
    {
        return DB::transaction(function () {
            return $this->delete();
        });
    }
}
