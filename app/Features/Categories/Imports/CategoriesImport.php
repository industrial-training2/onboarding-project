<?php

namespace App\Features\Categories\Imports;

use App\Features\Categories\Domain\Exports\CategoriesFailureExport;
use App\Features\Categories\Domain\Models\Category;
use App\Mail\CategoryImportFailureMail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Facades\Excel;

class CategoriesImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    protected $rules = [
        'name' => 'required|string|max:255',
        'description' => 'nullable|string',
        'is_active' => 'required',
    ];

    private function makeData(Collection $row)
    {
        return [
            "name" => trim($row["name"]),
            "description" => trim($row["description"]),
            "is_active" => trim($row["is_active"]) === "1",
        ];
    }

    public function collection(Collection $rows)
    {
        $failureData = [];
        $errorCols = [];
        foreach ($rows as $i => $row) {
            try {
                $data = $this->makeData($row);
                $validator = Validator::make($data, $this->rules, $this->customValidationMessages());
                if($validator->fails()) {
                    throw ValidationException::withMessages($validator->getMessageBag()->toArray());
                } else {
                    Category::create($data);
                }
            } catch (ValidationException $exception) {
                $errorCols[] = $exception->validator->messages()->messages();
                $failureData[] = $data;
                Log::info($exception->getMessage());
            }
        }
        if(empty(!$failureData)) {
            Excel::store(new CategoriesFailureExport($failureData, $errorCols), "CategoriesFailure.xlsx");
            Mail::to("darshbhavnani@gmail.com")->send(new CategoryImportFailureMail());
            Storage::delete('CategoriesFailure.xlsx');
        }
    }

    public function rules(): array
    {
        return $this->rules;
    }

    public function customValidationMessages()
    {
        return [
            'name.required' => 'name field is required',
            'name.max' => 'name should contain maximum 255 characters',
            'description' => 'description field should be a string',
            'is_active' => 'Is Active field is required',

        ];
    }
}
