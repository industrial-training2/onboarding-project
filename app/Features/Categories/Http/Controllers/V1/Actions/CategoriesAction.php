<?php

namespace App\Features\Categories\Http\Controllers\V1\Actions;


use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Http\Controllers\V1\Actions\ProductsAction;
use Illuminate\Support\Facades\DB;

class CategoriesAction
{
    public function persistCategory(array $data): Category
    {
        if($data["description"] == "")
        {
            unset($data["description"]);
        }
        return Category::persistCategory($data);
    }

    public function persistCategoryWithProducts(array $categoryData, array $productData): Category {
        $productAction = new ProductsAction;
        return DB::transaction(function () use ($categoryData, $productAction, $productData) {
            $category =  $this->persistCategory($categoryData);
            $productAction->persistProducts($productData, $category->id);
            return $category;
        });
    }

    public function updateCategory(Category $category, array $data): Category
    {
        if($data["description"] == "")
        {
            unset($data["description"]);
        }
        return $category->updateCategory($data);
    }

    public function deleteCategory(Category $category)
    {
        return $category->deleteCategory();
    }
}
