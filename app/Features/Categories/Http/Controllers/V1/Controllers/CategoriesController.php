<?php

namespace App\Features\Categories\Http\Controllers\V1\Controllers;

use App\DataTables\CategoriesDataTable;
use App\Features\Categories\Domain\Exports\CategoriesExport;
use App\Features\Categories\Domain\Exports\CategoriesSampleExport;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Categories\Http\Controllers\V1\Actions\CategoriesAction;
use App\Features\Categories\Http\Requests\CreateCategoryRequest;
use App\Features\Categories\Http\Requests\UpdateCategoryRequest;
use App\Features\Products\Http\Controllers\V1\Actions\ProductsAction;
use App\Http\Controllers\Controller;
use App\Jobs\ImportCategoryJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Nette\Schema\ValidationException;

class CategoriesController extends Controller
{

    public function __construct(private CategoriesAction $categoriesAction){}

    public function index(CategoriesDataTable $dataTable)
    {
        return $dataTable->render('categories.index');
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(CreateCategoryRequest $request)
    {
        try {
            $data = $request->only(['name', 'description', 'status', 'products']);
            $data['is_active'] = $data['status']==1;
            if(array_key_exists("products", $data)){
                $productData = $data['products'];
                $category = $this->categoriesAction->persistCategoryWithProducts($data, $productData);
            } else {
                $category =  $this->categoriesAction->persistCategory($data);
            }
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while saving details!");
            return redirect()->route("categories.index");
        }
        return redirect()->route("categories.index");
    }

    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    public function update(Category $category, UpdateCategoryRequest $request)
    {
        try {
            $data = $request->only(['name', 'description', 'status']);
            $data['is_active'] = $data['status']==1;

            $this->categoriesAction->updateCategory($category, $data);
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while updating details!");
            return redirect()->route("categories.index");
        }
        return redirect()->route("products.index");
    }

    public function delete(Category $category)
    {
        try {
            $this->categoriesAction->deleteCategory($category);
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Cannot delete this category as it has products associated with it!");
        }
        return redirect()->route("categories.index");
    }

    public function import(Request $request) {
        $file = $request->file('file');
        $storedFile = $file->storeAs(
            "temp/import-files",
            "temp".uniqid().".".$file->getClientOriginalExtension()
        );

        if(!$file) {
            Log::error("File Not Found!");
            session()->flash('error', 'File Not Found!');
            return redirect(route("categories.index"));
        }
        ImportCategoryJob::dispatch($storedFile);
        session()->flash("success", "Categories Import Successfully!");
        return redirect(route("categories.index"))->with('success', "Categories import has begun successfully, will be completed in some time!");
    }

    public function export(Request $request) {
        $data = [];
        if($request->has("name")) { $data["name"] = $request->get('name'); }
        if($request->has("description")) { $data["description"] = $request->get('description'); }
        if($request->has("status")) { $data["status"] = $request->get('status'); }
        if($request->has("startDate")) { $data["startDate"] = $request->get('startDate'); }
        if($request->has("endDate")) { $data["endDate"] = $request->get('endDate'); }
        return Excel::download(new CategoriesExport($data), 'categories.xlsx');
    }

    public function exportSampleSheet() {
        return Excel::download(new CategoriesSampleExport(), "sample-categories.xlsx");
    }
}
