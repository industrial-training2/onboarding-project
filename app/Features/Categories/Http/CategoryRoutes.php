<?php

use \Illuminate\Support\Facades\Route;

Route::resource("/categories", \App\Features\Categories\Http\Controllers\V1\Controllers\CategoriesController::class)->except('show');

Route::delete('categories/{category}/delete', [\App\Features\Categories\Http\Controllers\V1\Controllers\CategoriesController::class, 'delete'])->name("categories.delete");

Route::get("/categories/export", [\App\Features\Categories\Http\Controllers\V1\Controllers\CategoriesController::class, "export"])->name("categories.export");

Route::get("/categories/product-data", [\App\Features\Categories\Http\Controllers\V1\Controllers\CategoriesController::class, 'productData'])->name("categories.productData");
Route::post("categories.import", [\App\Features\Categories\Http\Controllers\V1\Controllers\CategoriesController::class, "import"])->name("categories.import");
Route::get("/categories/import/export-sample-sheet", [\App\Features\Categories\Http\Controllers\V1\Controllers\CategoriesController::class, "exportSampleSheet"])->name("categories.exportSampleSheet");
