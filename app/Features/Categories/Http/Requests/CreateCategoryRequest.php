<?php

namespace App\Features\Categories\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string',
            'is_active' => 'boolean',
        ];
    }

    public function messages(): array {
        return [
          'name.required' => 'The name field is required.',
          'name.string' => 'The name must be a string.',
          'description.string' => 'The description must be a string.',
          'is_active.boolean' => 'The status must be true or false.',
        ];
    }
}
