<?php

namespace App\Features\OrderItems\Domain\Models;

use App\Features\OrderItems\Domain\Models\Constants\OrderItemsConstants;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Product;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use App\Observers\OrderItemObserver;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Support\Facades\DB;

#[ObservedBy([OrderItemObserver::class])]
class OrderItem extends BaseModel implements OrderItemsConstants
{
    private array $custom_attributes;

    public function order() {
        $this->belongsTo(Order::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function getOldQuantityAttribute() {
        return $this->custom_attributes["old_quantity"];
    }

    public function setOldQuantityAttribute($value) {
        $this->custom_attributes["old_quantity"] = $value;
    }

    public static function persistOrderItem(array $data) {
        Utils::validateOrThrow($data, self::persistRules());
        if($data["quantity"] > Product::find($data["product_id"])->stock) {
            throw new \Exception("Quantity Ordered is greater than the available quantity");
        }
        return DB::transaction(function () use ($data) {
            $orderItem = OrderItem::create($data);
            return $orderItem;
        });
    }

    public function updateOrderItem(array $data) {
        Utils::validateOrThrow($data, self::updateRules());
        if($data["quantity"] > (Product::find($data["product_id"])->stock + $this->quantity)) {
            throw new \Exception("Quantity Ordered is greater than the available quantity");
        }
        return DB::transaction(function () use ($data) {
            $previousStock = $this->quantity;
            $this->setOldQuantityAttribute($this->quantity);
            $this->update($data);
            return $this;
        });
    }

    public function deleteOrderItem() {
        DB::transaction(function () {
           $this->product->increment('stock', $this->quantity);
           $this->delete();
        });
    }
    public static function persistRules(): array
    {
        return self::PERSIST_VALIDATION_RULES;
    }

    public static function updateRules(): array
    {
        return self::UPDATE_VALIDATION_RULES;
    }

    public function cancel() {
        return DB::transaction(function () {
           $quantity = $this->quantity;
           $this->product->increment('stock', $quantity);
           $this->quantity = 0;
           $this->saveQuietly();
        });
    }
}
