<?php

namespace App\Features\OrderItems\Domain\Models\Constants;

interface OrderItemsConstants
{
    public const PERSIST_VALIDATION_RULES = [
        'order_id' => 'required|exists:orders,id',
        'product_id' => 'required|exists:products,id',
        'quantity' => 'required',
        'unit_price' => 'required',
    ];

    public const UPDATE_VALIDATION_RULES = [
        'product_id' => 'required|exists:products,id',
        'quantity' => 'required',
        'unit_price' => 'required',
    ];
}
