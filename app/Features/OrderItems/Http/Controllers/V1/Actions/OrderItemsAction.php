<?php

namespace App\Features\OrderItems\Http\Controllers\V1\Actions;

use App\Features\OrderItems\Domain\Models\OrderItem;
use App\Features\Orders\Domain\Models\Order;
use Illuminate\Database\Eloquent\Collection;

class OrderItemsAction
{
    public function persistOrderItem(int $id, array $data) {
        for($i=0; $i<count($data); $i++) {
            $data[$i]['order_id'] = $id;
            $orderItems[] = OrderItem::persistOrderItem($data[$i]);
        }
        return $orderItems;
    }

    public function updateOrderItems(Collection $orderItems, array $data, int $orderId) {
        $original_count = count($orderItems);
        $new_count = count($data);
        if($original_count === $new_count) {
            foreach ($orderItems as $i => $orderItem) {
                $orderItem->updateOrderItem($data[$i]);
            }

        } elseif ($original_count > $new_count) {
            $count = 0;
            foreach ($orderItems as $i => $orderItem) {
               if($count < $new_count) {
                   $orderItem->updateOrderItem($data[$i]);
               } else {
                   $orderItem->deleteOrderItem();
               }
               $count++;
            };
        } else {
            $count = 0;
            foreach ($orderItems as $i => $orderItem) {
                $orderItem->updateOrderItem($data[$i]);
                $count++;
            }
            for($i = 0; $i < $new_count - $original_count; $i++) {
                $data[$count]['order_id'] = $orderId;
                OrderItem::persistOrderItem($data[$count++]);
            }

        }

    }

    public function cancelOrderItems(Collection $orderItems) {
        $orderItems->each(function(OrderItem $orderItem) {
           $orderItem->cancel();
        });
    }

    public function calculateNetTotal(array $orderItems) {
        $netTotal = 0;
        $collection = collect($orderItems)->flatten();
        foreach ($collection as $i => $orderItem) {
            $netTotal += $orderItem->unit_price * $orderItem->quantity;
        }
        return $netTotal;
    }
}
