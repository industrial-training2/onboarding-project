<?php

namespace App\Features\Products\Domain\Exports;

use App\Features\Categories\Domain\Exports\CategorySample;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ProductsSampleExport implements WithMultipleSheets, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function sheets(): array
    {
        $sheets = [];
        $sheets[0] = new ProductSkeletonSheet();

        $sheets[1] = new CategorySample();
        return $sheets;
    }
}
