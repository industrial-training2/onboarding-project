<?php

namespace App\Features\Products\Domain\Exports;

use App\Features\Products\Domain\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ProductsExport implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private Collection $products;
    public function __construct(array $data = []) {
        if(empty($data)) {
            $this->products = Product::all();
        }
        $queryBuilder = Product::query();
        if(isset($data["name"])) {
            $queryBuilder->where("name", "like", "%" . $data["name"] . "%");
        }
        if(isset($data["price"])) {
            $queryBuilder->where("price", "like", "%" . $data["price"] . "%");
        }
        if (isset($data["description"])) {
            $queryBuilder->where("description", "like", "%" . $data["description"] . "%");
        }
        if(isset($data["stock"])) {
            $queryBuilder->where("stock", "like", "%" . $data["stock"] . "%");
        }
        if(isset($data["status"])) {
            $queryBuilder->where("is_active", $data["status"]);
        }
        if(isset($data["category"])) {
            $queryBuilder->where("category_id", $data["category"]);
        }
        if(isset($data["startDate"])) {
            $queryBuilder->where("created_at", ">=", $data["startDate"]);
        }
        if(isset($data["endDate"])) {
            $endDate = date("Y-m-d", strtotime($data["endDate"]."+1 days"));
            $queryBuilder->where("created_at", "<=", $endDate);
        }
        $this->products = $queryBuilder->get();
    }
    public function collection()
    {
        return $this->products;
    }

    public function headings(): array
    {
        return [
            "ID",
            "Name",
            "Description",
            "Price",
            "Stock",
            "Is Active",
            'category_id',
            'created_at',
            'updated_at',
        ];
    }

    public function map($row): array
    {
        $data = $row->toArray();
        $data["is_active"] = $row->is_active ? "1" : "0";
        $data["created_at"] = date_format($row->created_at, "Y-m-d");
        $data["updated_at"] = date_format($row->updated_at, "Y-m-d");
        return $data;
    }
}
