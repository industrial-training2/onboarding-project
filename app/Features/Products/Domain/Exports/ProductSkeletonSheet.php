<?php

namespace App\Features\Products\Domain\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class ProductSkeletonSheet implements  FromCollection, WithHeadings, ShouldAutoSize, WithTitle
{

    public function headings(): array
    {
        return [
            "Name",
            "Description",
            "Price",
            "Stock",
            "Is Active",
            "Category_id",
        ];
    }


    public function collection()
    {
        return collect();
    }

    public function title(): string
    {
        return "Products";
    }
}
