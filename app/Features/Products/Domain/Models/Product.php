<?php

namespace App\Features\Products\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use App\Features\OrderItems\Domain\Models\OrderItem;
use App\Features\Products\Domain\Models\Constants\ProductConstants;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Database\Factories\ProductsFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends BaseModel implements ProductConstants
{
    protected static function newFactory()
    {
        return ProductsFactory::new();
    }

    public function order_item() {
        return $this->hasMany(OrderItem::class);
    }

    public function scopeActive(Builder $query) {
        $query->where("is_active", true);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public static function persistProduct($data): self
    {
        Utils::validateOrThrow($data, self::persistRules());
        return DB::transaction(function () use ($data) {
            return self::create($data);
        });
    }

    public function updateProduct(array $data): self
    {
        Utils::validateOrThrow($data, self::updateRules());
        DB::transaction(function () use ($data) {
           $this->update($data);
        });
        return $this;
    }

    public function deleteProduct(): bool
    {
        return DB::transaction(function () {
            return $this->delete();
        });
    }
    public static function persistRules(): array
    {
        return self::PERSIST_VALIDATION_RULES;
    }

    public static function updateRules(): array
    {
        return self::UPDATE_VALIDATION_RULES;
    }
}
