<?php

namespace App\Features\Products\Domain\Models\Constants;

interface ProductConstants
{
    public const PERSIST_VALIDATION_RULES = [
        'name' => 'required|string|max:255',
        'description' => 'nullable|string',
        'price' => 'required',
        'stock' => 'required',
        'is_active' => 'boolean',
        'category_id' => 'required|exists:categories,id',
    ];

    public const UPDATE_VALIDATION_RULES = [
        'name' => 'required|string|max:255',
        'description' => 'nullable|string',
        'price' => 'required',
        'stock' => 'required',
        'is_active' => 'boolean',
        'category_id' => 'required|exists:categories,id',
    ];
}
