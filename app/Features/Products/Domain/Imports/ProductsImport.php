<?php

namespace App\Features\Products\Domain\Imports;

use App\Features\Products\Domain\Exports\ProductsFailureExport;
use App\Mail\ImportFailure;
use App\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Facades\Excel;

class ProductsImport implements ToCollection, WithHeadingRow
{

    protected $rules = [
        'name' => 'required|string|max:255',
        'description' => 'nullable|string',
        'price' => 'required|numeric|',
        'stock' => 'required|numeric',
        'is_active' => 'required|boolean',
        'category_id' => 'required',
    ];

    private function makeData(Collection $row)
    {
        return [
            "name" => trim($row["name"]),
            "description" => $row["description"],
            "price" => trim($row["price"]),
            "stock" => trim($row["stock"]),
            "category_id" => trim($row["category_id"]),
            "is_active" => trim($row["is_active"]) === "1",
        ];
    }

    public function collection(Collection $rows)
    {
        $failureData = [];
        $errorCols = [];
        foreach ($rows as $i => $row) {
            try {
                $data = $this->makeData($row);
                $validator = Validator::make($data, $this->rules, $this->customValidationMessages());
                if($validator->fails()) {
                    throw ValidationException::withMessages($validator->getMessageBag()->toArray());
                } else {
                    $product = \App\Features\Products\Domain\Models\Product::create($data);
                }
            } catch (ValidationException $exception) {
                $errorCols[] = $exception->validator->messages()->messages();
                $failureData[] = $data;
                Log::info($exception->getMessage());
            }
        }
        if(!empty($failureData)) {
            Excel::store(new ProductsFailureExport($failureData, $errorCols), "ProductFailure.xlsx");
            Mail::to("darshbhavnani@gmail.com")->send(new ImportFailure("ProductFailure.xlsx"));
            Storage::delete('ProductFailure.xlsx');
        }
    }

    public function rules(): array
    {
        return $this->rules;
    }

    public function customValidationMessages()
    {
        return [
            'name.required' => 'name field is required',
            'name.max' => 'name should contain maximum 255 characters',
            'description' => 'description field should be a string',
            'price' => 'price is required',
            'price.numeric' => 'price should be numeric',
            'stock' => 'stock is required',
            'stock.numeric' => 'stock  should be numeric',
            'is_active' => 'Is Active field is required',
            'category_id' => 'category_id field is required',
        ];
    }
}
