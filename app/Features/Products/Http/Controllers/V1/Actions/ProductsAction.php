<?php

namespace App\Features\Products\Http\Controllers\V1\Actions;

use App\Features\Products\Domain\Models\Product;
use App\Helpers\Services\Utils;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ProductsAction
{
    public function persistProducts(array $data, int $id=0): array
    {
        $count = count($data);
        $productData = [];
        for($i=0; $i<$count; $i++){
            $productData["name"] = $data[$i]["name"];
            $productData["price"] = $data[$i]["price"];
            $productData["stock"] = $data[$i]["stock"];
            $productData["description"] = $data[$i]["description"];
            $productData["is_active"] = $data[$i]["status"] == 1;
            if($id!=0)
                $productData["category_id"] = $id;
            else
                $productData["category_id"] = $data[$i]["category_id"];
            $validator = Validator::make($productData, $this->rules(), $this->messages());
            if($validator->fails()) {
                throw ValidationException::withMessages($validator->getMessageBag()->toArray());
            }
            $products[$i] = Product::persistProduct($productData);
        }
        return $products;
    }

    private function rules(): array {
        return [
            'products.*.name' => 'required|string|max:255',
            'products.*.description' => 'nullable|string',
            'products.*.price' => 'required|numeric',
            'products.*.stock' => 'required|numeric|min:0',
            'products.*.category_id' => 'required|exists:categories,id',
        ];
    }

    private function messages(): array {
        return [
            'products.*.name' => 'Product name is required',
            'products.*.description' => 'Description should be alphanumeric',
            'products.*.price' => 'Price is required and should be numeric',
            'products.*.stock' => 'Stock is required and should be numeric',
            'products.*.category_id' => 'Category id is required',
        ];
    }

    public function updateProducts(Product $product, array $data): Product
    {
        $data["is_active"] = $data["status"] === "1";
        return $product->updateProduct($data);
    }

    public function deleteProduct(Product $product): bool
    {
        return $product->deleteProduct();
    }

    public function makeExportFailureData($failures): array {
        $data = [];
        $count = 0;
        $i=0;
        $errorMessages = [];
        foreach ($failures as $failure) {
            $productData = $failure->values();
            if(!in_array($productData, $data, false)) {
                $data[] = $productData;
                $count = 0;
                $errorMessages[$i] = array($failure->errors()[0]);
                $i++;
            } else {
                $count++;
                $errorMessages[$i-1] = array($errorMessages[$i-1][0], $failure->errors()[0]);
            }
        }
        return array($data, $errorMessages);
    }
}
