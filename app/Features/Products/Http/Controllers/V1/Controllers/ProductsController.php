<?php

namespace App\Features\Products\Http\Controllers\V1\Controllers;

use App\DataTables\ProductsDataTable;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Exports\ProductsExport;
use App\Features\Products\Domain\Exports\ProductsSampleExport;
use App\Features\Products\Domain\Models\Product;
use App\Features\Products\Http\Controllers\V1\Actions\ProductsAction;
use App\Features\Products\Http\Requests\CreateProductRequest;
use App\Features\Products\Http\Requests\UpdateProductRequest;
use App\Http\Controllers\Controller;
use App\Jobs\ImportProductJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use PHPUnit\Exception;

class ProductsController extends Controller
{
    public function __construct(private ProductsAction $productsAction) {
    }

    public function index(ProductsDataTable $dataTable)
    {
        $categories = Category::all();
        return $dataTable->render('products.index', compact('categories'));
    }

    public function create()
    {
        $categories = Category::active()->get();
        return view('products.create', compact('categories'));
    }

    public function store(CreateProductRequest $request)
    {
        try {
            $this->productsAction->persistProducts($request["products"]);
        } catch(Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while saving details!");
            return redirect()->view("products.index");
        }

        session()->flash("success", "Successfully created a product!");
        return redirect()->route("products.index");
    }

    public function edit(Product $product)
    {
        $categories = Category::where("is_active", true)->get();
        return view('products.edit', compact('product', 'categories'));
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        try {
            $dataExtractred = $request->only(["name", "description", "price", "stock", "status", "category_id"]);
            $this->productsAction->updateProducts($product, $dataExtractred);
        } catch(Exception $exception) {
            Log::info($exception);
            session()->flash("eror", "Error while updating details!");
        }
        return redirect()->route("products.index");
    }

    public function delete(Product $product)
    {
        $this->productsAction->deleteProduct($product);
        return redirect()->route("products.index");
    }

    public function import(Request $request) {
        $file = $request->file('file');
        $storedFile = $file->storeAs(
            "temp/import-files",
            "temp".uniqid().".".$file->getClientOriginalExtension()
        );

        if(!$file) {
            Log::error("File Not Found!");
            session()->flash('error', 'File Not Found!');
            return redirect(route("products.index"));
        }
        ImportProductJob::dispatch($storedFile);
        session()->flash("success", "Students Import Successfully!");
        return redirect(route("products.index"))->with('success', "Students import has begun successfully, will be completed in some time!");
    }

    public function export(Request $request) {
        $data = [];
        if($request->has("name")) { $data["name"] = $request->get('name'); }
        if($request->has("description")) { $data["description"] = $request->get('description'); }
        if($request->has("price")) { $data["price"] = $request->get('price'); }
        if($request->has("stock")) { $data["stock"] = $request->get('stock'); }
        if($request->has("category")) { $data["category"] = $request->get('category'); }
        if($request->has("status")) { $data["status"] = $request->get('status'); }
        if($request->has("startDate")) { $data["startDate"] = $request->get('startDate'); }
        if($request->has("endDate")) { $data["endDate"] = $request->get('endDate'); }
        return Excel::download(new ProductsExport($data), 'products.xlsx');
    }

    public function exportSampleSheet() {
        return Excel::download(new ProductsSampleExport(), "sample-products.xlsx");
    }
}
