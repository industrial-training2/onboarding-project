<?php

use App\Features\Products\Http\Controllers\V1\Controllers\ProductsController;
use \Illuminate\Support\Facades\Route;

Route::resource("/products", \App\Features\Products\Http\Controllers\V1\Controllers\ProductsController::class)->except("show");
Route::delete("/products/{product}/delete", [\App\Features\Products\Http\Controllers\V1\Controllers\ProductsController::class, "delete"])->name("products.delete");
Route::get("/products/export", [\App\Features\Products\Http\Controllers\V1\Controllers\ProductsController::class, "export"])->name("products.export");

Route::get("/products/product-data", [\App\Features\Products\Http\Controllers\V1\Controllers\ProductsController::class, 'productData'])->name("products.productData");
Route::post("products.import", [ProductsController::class, "import"])->name("products.import");
Route::get("/products/import/export-sample-sheet", [ProductsController::class, "exportSampleSheet"])->name("products.exportSampleSheet");
