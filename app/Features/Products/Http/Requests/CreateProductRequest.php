<?php

namespace App\Features\Products\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'products.*.name' => 'required|string|max:255',
            'products.*.description' => 'nullable|string',
            'products.*.price' => 'required|numeric',
            'products.*.stock' => 'required|numeric|min:0',
            'products.*.category_id' => 'required|exists:categories,id',
        ];
    }

    public function messages()
    {
        return [
            'products.*.name' => 'Product name is required',
            'products.*.description' => 'Description should be alphanumeric',
            'products.*.price' => 'Price is required and should be numeric',
            'products.*.stock' => 'Stock is required and should be numeric',
            'products.*.category_id' => 'Category id is required',
        ];
    }
}
