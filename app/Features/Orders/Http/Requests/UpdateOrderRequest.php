<?php

namespace App\Features\Orders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "name" => "required|string",
            "email" => "required|email",
            "net_total" => "nullable|numeric",
            "date" => "required",
            "orders.*.category" => "required",
            "orders.*.product_id" => "required",
            "orders.*.quantity" => "required|min:0",
            "orders.*.total_price" => "required",
            "orders.*.unit_price" => "required",
        ];
    }

    public function messages()
    {
        return [
            "name.string" => "Name must be string",
            "email.required" => "Email is required",
            "email.email" => "Invalid email",
            "net_total.numeric" => "Net total must be numeric",
            "date.required" => "Date is required",
            "orders.*.category.required" => "Category is required",
            "orders.*.product_id.required" => "Product is required",
            "orders.*.quantity.required" => "Quantity is required",
        ];
    }
}
