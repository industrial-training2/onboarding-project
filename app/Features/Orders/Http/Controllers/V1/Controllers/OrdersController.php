<?php

namespace App\Features\Orders\Http\Controllers\V1\Controllers;

use App\DataTables\OrderItemsDataTable;
use App\DataTables\OrdersDataTable;
use App\Features\Categories\Domain\Models\Category;
use App\Features\OrderItems\Http\Controllers\V1\Actions\OrderItemsAction;
use App\Features\Orders\Domain\Exports\OrderSampleExport;
use App\Features\Orders\Domain\Exports\OrdersExport;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Orders\Http\Controllers\V1\Actions\OrdersAction;
use App\Features\Orders\Http\Requests\CreateOrderRequest;
use App\Features\Orders\Http\Requests\ImportOrderRequest;
use App\Features\Orders\Http\Requests\UpdateOrderRequest;
use App\Features\Products\Domain\Models\Product;
use App\Http\Controllers\Controller;
use App\Jobs\ImportOrderJob;
use App\Jobs\OrderMailJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class OrdersController extends Controller
{
    public function __construct(private OrdersAction $ordersAction, private OrderItemsAction $orderItemsAction) {}

    public function index(OrdersDataTable $dataTable) {
        $products = Product::active()->get();
        return $dataTable->render('orders.index', compact('products'));
    }

    public function create() {
        $categories = Category::active()->get();
        return view("orders.create", compact("categories"));
    }

    public function getProducts(Request $request) {
        $category_id = $request['category_id'];
        $products = $this->ordersAction->getProductsFromCategory($category_id);
        return json_encode(($products));
    }

    public function show(int $orderId, OrderItemsDataTable $orderItemsDataTable) {
        return $orderItemsDataTable->with('orderId', $orderId)->render('orders.show');
    }

    public function edit(Order $order) {
        $categories = Category::active()->get();
        return view("orders.edit", compact("order", "categories"));
    }


    public function store(CreateOrderRequest $request) {
        try {
            $data["customer_name"] = $request["name"];
            $data["order_date"] = $request["date"];
            $data["customer_email"] = $request["email"];
            $data["net_total"] = $request["net_total"];
            $orderItems = $request["orders"];
            $order = $this->ordersAction->persistOrder($data, $this->orderItemsAction, $orderItems);

            OrderMailJob::dispatch($order);
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while saving details!");
            return redirect()->view("orders.index");
        }
        session()->flash("success", "Successfully created an order!");
        return redirect()->back();
    }

    public function update(Order $order, UpdateOrderRequest $request) {
        try {
            $data["customer_name"] = $request["name"];
            $data["order_date"] = $request["date"];
            $data["customer_email"] = $request["email"];
            $orderItems = $request["orders"];
            $this->ordersAction->updateOrder($order, $data);
            DB::transaction(function () use ($order, $orderItems) {
                $this->orderItemsAction->updateOrderItems($order->orderItems, $orderItems, $order->id);
            });
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while saving details!");
            return redirect()->route("orders.index");
        }
        session()->flash("success", "Successfully updated an order!");
        return redirect()->route("orders.index");
    }

    public function cancelOrder(Request $request, Order $order)
    {
        DB::transaction(function () use($order) {
            $order = $this->ordersAction->cancelOrder($order);
            $this->orderItemsAction->cancelOrderItems($order->orderItems);
        });
        OrderMailJob::dispatch($order);
        return redirect()->back();
    }

    public function importOrders(ImportOrderRequest $request) {
        $customerData["customer_name"] = $request["name"];
        $customerData["customer_email"] = $request["email"];
        $customerData["order_date"] = $request["date"];
        $file = $request->file('file');
        if(!$file) {
            Log::error("File Not Found!");
            session()->flash('error', 'File Not Found!');
            return redirect(route("orders.index"));
        }

        $storedFile = $file->storeAs(
            "temp/import-files",
            "temp".uniqid().".".$file->getClientOriginalExtension()
        );

        ImportOrderJob::dispatch($customerData, $storedFile);
        session()->flash("success", "Students Import Successfully!");
        return redirect(route("orders.index"))->with('success', "Orders import has begun successfully, will be completed in some time!");
    }

    public function export(Request $request) {
        $data = [];
        if($request->has("name")) { $data["name"] = $request->get('name'); }
        if($request->has("email")) { $data["email"] = $request->get('email'); }
        if($request->has("orderDate")) { $data["date"] = $request->get('orderDate'); }
        if($request->has("product")) { $data["product"] = $request->get('product'); }
        if($request->has("startDate")) { $data["startDate"] = $request->get('startDate'); }
        if($request->has("endDate")) { $data["endDate"] = $request->get('endDate'); }
        return Excel::download(new OrdersExport($data), 'orders.xlsx');
    }

    public function exportSampleOrder() {
        return Excel::download(new OrderSampleExport(), "sample-orders.xlsx");
    }
}
