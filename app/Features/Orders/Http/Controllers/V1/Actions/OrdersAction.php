<?php

namespace App\Features\Orders\Http\Controllers\V1\Actions;

use App\Features\Categories\Domain\Models\Category;
use App\Features\OrderItems\Http\Controllers\V1\Actions\OrderItemsAction;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Product;

class OrdersAction
{
    public function persistOrder(array $data, OrderItemsAction $orderItemsAction, array $orderItems): Order {
        $order = Order::persistOrder($data);
        $orderItemsAction->persistOrderItem($order->id, $orderItems);
        return $order;
    }

    public function updateOrder(Order $order, array $data) {
        return $order->updateOrder($data);
    }

    public function cancelOrder(Order $order) {
        return $order->cancelOrder();
    }

    public function updateNetTotal(Order $order, $net_total) {
        return $order->updateNetTotal($net_total);
    }

    public function getProductsFromCategory(int $category_id) {
        return Product::where("category_id", $category_id)->active()->get();
    }
}
