<?php

use App\Features\Orders\Http\Controllers\V1\Controllers\OrdersController;
use Illuminate\Support\Facades\Route;

Route::resource("/orders", \App\Features\Orders\Http\Controllers\V1\Controllers\OrdersController::class)->except(['edit', 'update']);
Route::get("/orders/{id}/edit",[OrdersController::class, 'edit'])->name("orders.edit")->middleware(\App\Http\Middleware\EnsureOrderStatus::class);
Route::put("/orders/{id}",[OrdersController::class, 'update'])->name("orders.update")->middleware(\App\Http\Middleware\EnsureOrderStatus::class);
//Route::get("/orders/{order}", [OrdersController::class, "show"])->name("orders.show");
Route::post("/orders/get-products", [\App\Features\Orders\Http\Controllers\V1\Controllers\OrdersController::class, "getProducts"])->name("orders.getProducts");
Route::bind('id', function ($id) {
   return \App\Features\Orders\Domain\Models\Order::with("order_items.product.category.products")->find($id);
});

Route::get('orders/export/sample-order', [OrdersController::class, "exportSampleOrder"])->name("orders.exportSampleOrder");
Route::post('orders/import', [OrdersController::class, 'importOrders'])->name("orders.import");
Route::get("orders/export/export-data", [OrdersController::class, 'export'])->name('orders.export');
Route::put('orders/{id}/cancel-order', [OrdersController::class, 'cancelOrder'])->name('orders.cancel')->middleware(\App\Http\Middleware\EnsureOrderStatus::class);
Route::post('orders/filter/product', [OrdersController::class, 'filterProducts'])->name("orders.getOrders");
