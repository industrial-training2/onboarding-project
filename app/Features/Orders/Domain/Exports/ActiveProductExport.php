<?php

namespace App\Features\Orders\Domain\Exports;

use App\Features\Products\Domain\Models\Product;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ActiveProductExport implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping
{
    private array $data;
    public function __construct(){
    }

    public function collection()
    {
//        return DB::table("products")
//            ->join("categories",
//                "products.category_id",
//                "=",
//                "categories.id")
//            ->select("categories.name AS category_name",
//                "products.id AS product_id",
//                "products.name AS product_name")
//            ->orderBy('products.id', "ASC")
//            ->get();
        return Product::with('category')->where('is_active', true)->orderBy("products.id")->get();
    }

    public function headings(): array
    {
        return [
            "category_name",
            "product_id",
            "product_name",
        ];
    }

    public function map($row): array
    {
        $data["category_name"] = $row->category->name;
        $data["product_id"] = $row->id;
        $data["product_name"] = $row->name;
        return $data;
    }
}
