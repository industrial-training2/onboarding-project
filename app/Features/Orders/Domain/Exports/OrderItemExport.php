<?php

namespace App\Features\Orders\Domain\Exports;

use App\Features\OrderItems\Domain\Models\OrderItem;
use App\Features\Orders\Domain\Models\Order;
use Maatwebsite\Excel\Concerns\FromArray;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class OrderItemExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle, WithMapping
{
    private $orderItems;
    private $orders;
    public function __construct(Collection $orders)
    {
        $this->orders = $orders;
    }

    public function collection()
    {
        $items = [];
        foreach ($this->orders as $order) {
            $items[] = $order->orderItems;
        }

        $this->orderItems = collect($items)->flatten();
        return $this->orderItems;

    }

    public function headings(): array
    {
        return [
            "order_id",
            "product_name",
            "quantity",
            "unit_price",
            "created_at",
            "updated_at"
        ];
    }

    public function title(): string
    {
        return "Order Items";
    }

    public function map($row): array
    {
        $data["order_id"] = $row->order_id;
        $data["product_name"] = $row->product->name;
        $data["quantity"] = $row->quantity;
        $data["unit_price"] = $row->unit_price;
        $data["created_at"] = date_format($row->created_at, "Y-m-d");
        $data["updated_at"] = date_format($row->updated_at, "Y-m-d");

        return $data;
    }
}
