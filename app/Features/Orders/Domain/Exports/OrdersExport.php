<?php

namespace App\Features\Orders\Domain\Exports;

use App\Features\OrderItems\Domain\Models\OrderItem;
use App\Features\Orders\Domain\Models\Order;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OrdersExport implements WithMultipleSheets
{
    private Collection $orders;
    public function __construct(array $data = [])
    {
        if(empty($data)) {
            $this->orders = Order::all();
        }
        $queryBuilder = Order::query();
        if(isset($data["name"])) {
            $queryBuilder->where("customer_name", "like", "%" . $data["name"] . "%");
        }
        if(isset($data["email"])) {
            $queryBuilder->where("customer_email", "like", "%" . $data["email"] . "%");
        }
        if (isset($data["date"])) {
            $queryBuilder->where("order_date", "like", "%" . $data["date"] . "%");
        }
        if (isset($data["product"])) {
            $orderIds = OrderItem::where("product_id", $data["product"])->pluck("order_id")->toArray();
            $queryBuilder->whereIn("id", $orderIds);
        }
        if(isset($data["startDate"])) {
            $queryBuilder->where("created_at", ">=", $data["startDate"]);
        }
        if(isset($data["endDate"])) {
            $endDate = date("Y-m-d", strtotime($data["endDate"]."+1 days"));
            $queryBuilder->where("created_at", "<=", $endDate);
        }
        $this->orders = $queryBuilder->get();
    }

    public function sheets(): array
    {
        $sheets[] = new OrdersSheetExport($this->orders);
        $sheets[] = new OrderItemExport($this->orders);
        return $sheets;
    }
}
