<?php

namespace App\Features\Orders\Domain\Exports;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class OrdersSheetExport implements FromCollection, ShouldAutoSize, WithHeadings, WithTitle, WithMapping
{
    private Collection $orders;
    public function __construct(Collection $orders)
    {
        $this->orders = $orders;
    }

    public function collection()
    {
        return $this->orders;
    }

    public function headings(): array
    {
        return [
            "order_id",
            "customer_name",
            "customer_email",
            "net_total",
            "order_date",
            "order_status",
            "created_at",
            "updated_at"
        ];
    }

    public function title(): string
    {
        return "Orders";
    }
    public function map($row): array
    {
        $data = $row->toArray();
        $data["created_at"] = date_format($row->created_at, "Y-m-d");
        $data["updated_at"] = date_format($row->updated_at, "Y-m-d");
        return $data;
    }
}
