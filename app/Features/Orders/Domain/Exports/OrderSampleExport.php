<?php

namespace App\Features\Orders\Domain\Exports;

use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OrderSampleExport implements WithMultipleSheets
{

    public function sheets(): array
    {
        $sheets[] = new OrderItemHeaderExport();
        $sheets[] = new ActiveProductExport();
        return $sheets;
    }
}
