<?php

namespace App\Features\Orders\Domain\Imports;

use App\Features\OrderItems\Http\Controllers\V1\Actions\OrderItemsAction;
use App\Features\Orders\Domain\Exports\OrdersFailureExport;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Orders\Http\Controllers\V1\Actions\OrdersAction;
use App\Features\Products\Domain\Models\Product;
use App\Mail\ImportFailure;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Facades\Excel;

class OrdersImport implements ToCollection, WithHeadingRow
{
    private $customer_data;
    private $ordersAction;
    private $orderItemsAction;

    public function __construct(array $customer_data)
    {
        $this->customer_data = $customer_data;
        $this->ordersAction = new OrdersAction();
        $this->orderItemsAction = new OrderItemsAction();
    }

    protected $rules = [
        'product_id' => 'required|exists:products,id',
        'order_id' => 'required',
    ];

    private function makeData(Collection $row, Order $order): array
    {
        return [
            'order_id' => $order->id,
            'product_id' => $row['product_id'],
            'quantity' => $row['quantity'],
        ];
    }

    public function collection(Collection $rows)
    {
        $netTotal = 0;
        $failureData = [];
        $errorCols = [];
        $orderItems = [];
        $order = $this->ordersAction->persistOrder($this->customer_data);
        foreach ($rows as $i => $row) {
            try {
                $data = [];
                if(empty($row["product_id"])) {
                    throw ValidationException::withMessages(array("Product ID is required"));
                }
                $data = $this->makeData($row, $order);
                $validator = Validator::make($data, $this->rules, $this->customValidationMessages());
                if($validator->fails()) {
                    throw ValidationException::withMessages($validator->getMessageBag()->toArray());
                }
                $product = Product::find($row['product_id']);
                $data["unit_price"] = $product->price;
                $productRules = $this->rules;
                $productRules['quantity'] = 'required|numeric|min:0|max:'.$product->stock;
                $validator = Validator::make($data, $productRules, $this->customValidationMessages());
                if($validator->fails()) {
                    throw ValidationException::withMessages($validator->getMessageBag()->toArray());
                }
                $orderItems[] = $this->orderItemsAction->persistOrderItem($order->id, array($data));
            } catch (ValidationException $exception) {
                $errorCols[] = $exception->validator->messages()->messages();
                $failureData[] = $row->toArray();
                Log::info($exception->getMessage());
            }
        }
        $netTotal = $this->orderItemsAction->calculateNetTotal($orderItems);
        $this->ordersAction->updateNetTotal($order, $netTotal);
        if(!empty($failureData)) {
            Excel::store(new OrdersFailureExport($failureData, $errorCols), "OrderFailure.xlsx");
            Mail::to("darshbhavnani@gmail.com")->send(new ImportFailure("OrderFailure.xlsx"));
            Storage::delete('ProductFailure.xlsx');
        }
    }

    public function rules(): array
    {
        return $this->rules;
    }

    public function customValidationMessages()
    {
        return [
            'product_id.exists' => 'The product ID should exists in the products data',
            'quantity.numeric' => 'Quantity should be in number',
            'quantity.required' => 'Quantity is required',
            'product_id.required' => 'Product ID is required',
            'quantity.max' => 'Please enter the quantity considering the stock of the product',
        ];
    }
}
