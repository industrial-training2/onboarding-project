<?php

namespace App\Features\Orders\Domain\Models;

use App\Features\OrderItems\Domain\Models\OrderItem;
use App\Features\Orders\Domain\Models\Constants\OrderConstants;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Illuminate\Support\Facades\DB;

class Order extends BaseModel implements OrderConstants
{


    public function orderItems() {
        return $this->hasMany(OrderItem::class);
    }

    public static function persistOrder($data): self {
        Utils::validateOrThrow($data, self::persistRules());
        return DB::transaction(function () use ($data) {
           return Order::create($data);
        });
    }

    public function updateOrder(array $data) {
        Utils::validateOrThrow($data, self::updateRules());
        return DB::transaction(function () use ($data) {
           return $this->update($data);
        });
    }

    public function order_items() {
        return $this->hasMany(OrderItem::class);
    }
    public static function persistRules(): array
    {
        return self::PERSIST_VALIDATION_RULES;
    }

    public static function updateRules(): array
    {
        return self::UPDATE_VALIDATION_RULES;
    }

    public function cancelOrder() {
       $this->order_status = "CANCELLED";
       $this->save();
        return $this;
    }

    public function updateNetTotal(int $net_total) {
        $this->net_total = $net_total;
        $this->save();
        return $this;
    }
}
