<?php

namespace App\Features\Orders\Domain\Models\Constants;

interface OrderConstants
{
    public const PERSIST_VALIDATION_RULES = [
        'customer_name' => 'required|string|max:255',
        'customer_email' => 'required|string|email|max:255',
        'net_total' => 'nullable|numeric',
        'order_date' => 'required',
    ];

    public const UPDATE_VALIDATION_RULES = [
        'customer_name' => 'required|string|max:255',
        'customer_email' => 'required|string|email|max:255',
        'net_total' => 'nullable|numeric',
        'order_date' => 'required',
    ];
}
