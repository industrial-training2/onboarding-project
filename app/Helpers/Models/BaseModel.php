<?php

namespace App\Helpers\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public static abstract function persistRules(): array;

    public static abstract function updateRules(): array;
}
