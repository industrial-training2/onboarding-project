<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    private const DATABASE_MIGRATIONS = [
        __DIR__.'/../Features/Categories/Migrations',
        __DIR__.'/../Features/Products/Migrations',
        __DIR__.'/../Features/Orders/Migrations',
        __DIR__.'/../Features/OrderItems/Migrations',
    ];
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(self::DATABASE_MIGRATIONS);
    }
}
