<?php

namespace App\Observers;


use App\Features\OrderItems\Domain\Models\OrderItem;
use App\Features\Products\Domain\Models\Product;
use Illuminate\Contracts\Events\ShouldHandleEventsAfterCommit;

class OrderItemObserver implements ShouldHandleEventsAfterCommit
{
    /**
     * Handle the OrderItem "created" event.
     */
    public function created(OrderItem $orderItem): void
    {
        $quantity = $orderItem->quantity;
        $product = $orderItem->product;
        $product->decrement('stock', $quantity);
    }

    /**
     * Handle the OrderItem "updated" event.
     */
    public function updated(OrderItem $orderItem): void
    {
        $oldQuantity = $orderItem->getOldQuantityAttribute();
        $newQuantity = $orderItem->quantity;
        $product = $orderItem->product;
        if($newQuantity > $newQuantity) {
            $difference = $newQuantity - $oldQuantity;
            $product->decrement('stock', $difference);
        } else {
            $difference = $oldQuantity - $newQuantity;
            $product->increment('stock', $difference);
        }
    }

    /**
     * Handle the OrderItem "deleted" event.
     */
    public function deleted(OrderItem $orderItem): void
    {
        //
    }

    /**
     * Handle the OrderItem "restored" event.
     */
    public function restored(OrderItem $orderItem): void
    {
        //
    }

    /**
     * Handle the OrderItem "force deleted" event.
     */
    public function forceDeleted(OrderItem $orderItem): void
    {
        //
    }
}
