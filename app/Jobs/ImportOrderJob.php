<?php

namespace App\Jobs;

use App\Features\Orders\Domain\Imports\OrdersImport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class ImportOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    private $customerData;
    private $storedFile;
    public function __construct(array $customerData, $storedFile)
    {
        $this->storedFile = $storedFile;
        $this->customerData = $customerData;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Excel::Import(new OrdersImport($this->customerData), $this->storedFile, 'public');
    }
}
