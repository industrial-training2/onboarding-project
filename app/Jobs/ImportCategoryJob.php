<?php

namespace App\Jobs;

use App\Features\Categories\Imports\CategoriesImport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class ImportCategoryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    private string $storedFile;
    public function __construct(string $storedFile){
        $this->storedFile = $storedFile;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Excel::Import(new CategoriesImport(), $this->storedFile, 'public');
    }
}
