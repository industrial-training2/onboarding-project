<?php

namespace App\DataTables;

use App\Features\OrderItems\Domain\Models\OrderItem;
use App\Features\Orders\Domain\Models\Order;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\Date;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class OrdersDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn("Actions", function(Order $order) {
                $show = '<a href="'.route('orders.show', $order->id).'" class="btn btn-sm btn-outline-cyan me-1">Show</a>';
                $display = $order->order_status !== 'PENDING' ? 'd-none' : '';
                $cancel = '<form action="'.route("orders.cancel", $order->id).'" class="d-inline me-1 '.$display.'" method="POST" ><input type="hidden" name="_token" value="'.csrf_token().'"/> <input type="hidden" name="_method" value="PUT"> <button type="submit" class="btn btn-sm d-inline btn btn-outline-danger">Cancel</button></form>';;
                $edit = '<a href="'.route("orders.edit", $order->id).'" class="btn btn-sm btn-outline-warning m-1 '.$display.'">Edit</a>';
                return $show.$edit.$cancel;
            })->filter(function ($query) {
                $productID = $this->request()->has("product_id") ? $this->request()->input("product_id") : null;
                if($productID != null) {
                    $orderIds = OrderItem::where("product_id", $productID)->pluck("order_id")->toArray();
                    $query->whereIn("id", $orderIds);
                }
            })->addColumn("created_at", function (Order $order) {
                return Carbon::parse($order->created_at)->diffForHumans();
            })->addColumn("updated_at", function (Order $order) {
                return Carbon::parse($order->created_at)->diffForHumans();
            })->filter(function ($query) {
                $name = request()->input('name');
                $email = request()->input('email');
                $orderDate = request()->input('order_date');
                $productID = request()->input('product_id');
                $startDate = $this->request()->input('startDate');
                $endDate = $this->request()->input('endDate');
                if($name != null) {
                    $query->where("name", "like", "%{$name}%");
                }
                if($email != null) {
                    $query->where("email", "like", "%{$email}%");
                }
                if($orderDate != null) {
                    $query->where("order_date", $orderDate);
                }
                if($productID != null) {
                    $orderIds = OrderItem::where("product_id", $productID)->pluck("order_id")->toArray();
                    $query->whereIn("id", $orderIds);
                }
                if($startDate != null) {
                    $query->where("created_at", ">=" ,$startDate);
                }
                if($endDate != null) {
                    $endDate = date('Y-m-d', strtotime($endDate."+1 days"));
                    $query->where("created_at", "<=", $endDate);
                }
            })->rawColumns(["Actions"]);
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Order $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('orders-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->parameters([
                        'lengthMenu' => [
                            [10, 20, 30, 40],
                            ["10 rows", "20 rows", "30 rows", "50 rows", "100 rows"],
                        ],
                        "order" => [0, "desc"],
                        "scrollY" => false,
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('id'),
            Column::make('customer_name'),
            Column::make('customer_email'),
            Column::make('order_date'),
            Column::make('net_total'),
            Column::make('order_status'),
            Column::make("created_at")->title("Created At"),
            Column::make("updated_at")->title("Updated At"),
            Column::make("Actions"),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Orders_' . date('YmdHis');
    }
}
