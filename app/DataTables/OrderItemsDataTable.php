<?php

namespace App\DataTables;

use App\Features\OrderItems\Domain\Models\OrderItem;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class OrderItemsDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('product_id', function(OrderItem $orderItem) {
                return $orderItem->product->name;
            })->editColumn('created_at', function(OrderItem $orderItem) {
                return Carbon::parse($orderItem->created_at)->diffForHumans();
            })->editColumn('updated_at', function(OrderItem $orderItem) {
                return Carbon::parse($orderItem->updated_at)->diffForHumans();
            })
            ->filter(function ($query) {
                $orderId = $this->request()->route('order');
                if($orderId != null) {
                    $query->where('order_id', $orderId);
                }
            });
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(OrderItem $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('orderitems-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('lrtrp')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->parameters([
                        'lengthMenu' => [
                            [10, 20, 30, 40],
                            ["10 rows", "20 rows", "30 rows", "50 rows", "100 rows"],
                        ],
                        "order" => [0, "desc"],
                        "scrollY" => false,
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('id'),
            Column::make('order_id'),
            Column::make('product_id'),
            Column::make('quantity'),
            Column::make('unit_price'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'OrderItems_' . date('YmdHis');
    }
}
