<?php

namespace App\DataTables;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Product;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->editColumn("is_active", function (Product $product) {
                $badgeColor = $product->is_active ? 'text-success' : 'text-danger';
                $text = $product->is_active ? 'Active' : 'Inactive';
                return "<span class='badge $badgeColor'>{$text}</span>";
            })->editColumn("category_id", function (Product $product) {
                return $product->category->name;
            })->addColumn("Actions", function (Product $product) {
                    $class = '<a href="'.route("products.edit", $product).'" class="btn btn-warning btn-sm m-1">Edit</a>
                              <form action="'.route("products.delete", $product).'" class="d-inline m-1" method="POST" ><input type="hidden" name="_token" value="'.csrf_token().'"/> <input type="hidden" name="_method" value="DELETE"> <button type="submit" class="btn btn-sm d-inline btn-danger">Delete</button></form>';
                    return $class;
            })->editColumn("created_at", function (Product $product) {
                return date_format($product->created_at, "Y-m-d");
            })->editColumn("updated_at", function (Product $product) {
                return date_format($product->updated_at, "Y-m-d");
            })->filter(function ($query) {
                $name = request()->input('name');
                $description = request()->input('description');
                $price = request()->input('price');
                $stock = request()->input('stock');
                $category = request()->input('category');
                $status = request()->input('status');
                $startDate = $this->request()->input('startDate');
                $endDate = $this->request()->input('endDate');
                if($name != null) {
                    $query->where("name", "like", "%{$name}%");
                }
                if($description != null) {
                    $query->where("description", "like", "%{$description}%");
                }
                if($status != null) {
                    $query->where("is_active", $status);
                }
                if($price != null) {
                    $query->where("price", "like", "%{$price}%");
                }
                if($stock != null) {
                    $query->where("stock", "like", "%{$stock}%");
                }
                if($category != null) {
                    $query->where("category_id", $category);
                }
                if($startDate != null) {
                    $query->where("created_at", ">=" ,$startDate);
                }
                if($endDate != null) {
                    $endDate = date('Y-m-d', strtotime($endDate."+1 days"));
                    $query->where("created_at", "<=", $endDate);
                }
            })->rawColumns(['is_active', 'Actions', 'category_id']);
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Product $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('products-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->parameters([
                        'lengthMenu' => [
                            [10, 20, 30, 40],
                            ["10 rows", "20 rows", "30 rows", "50 rows", "100 rows"],
                        ],
                        "order" => [0, "desc"],
                        "scrollY" => false,
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('id'),
            Column::make('name'),
            Column::make('description'),
            Column::make('price'),
            Column::make('stock'),
            Column::make('category_id')->title("category"),
            Column::make('is_active')->title("status"),
            Column::make("created_at")->title("Created at"),
            Column::make("updated_at")->title("Updated at"),
            Column::make("Actions"),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Products_' . date('YmdHis');
    }
}
