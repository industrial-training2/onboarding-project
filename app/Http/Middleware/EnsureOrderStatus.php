<?php

namespace App\Http\Middleware;

use App\Features\Orders\Domain\Models\Order;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EnsureOrderStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $order = $request->route('id');
        if($order->order_status === 'PENDING')
            return $next($request);
        abort(401, "");
    }
}
