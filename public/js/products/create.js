const product_form = $("#productForm")[0].innerHTML;
class ProductForm {
    static count = 1;
    static getProductForm() {
        let product_form_mod;
        product_form_mod = product_form.replaceAll('products[0]', 'products['+ ProductForm.count + ']');
        product_form_mod = product_form_mod.replaceAll('innerProduct-no', 'innerProduct-' + ProductForm.count);
        return product_form_mod;
    }
}

$("#add").click(function () {
    $("#productForm").append(ProductForm.getProductForm());
    ProductForm.count++;
    $("#deleteProduct").removeClass("d-none");
});

$("#deleteProduct").click(function () {
    $("#innerProduct-" + (ProductForm.count-1)).remove();
    ProductForm.count--;
    $("#innerProduct-1").length === 0 ? $("#deleteProduct").addClass("d-none") : "";
});

jQuery.validator.addClassRules("name", {
    required: true,
    minlength: 2,
    maxlength: 255,
    normalizer: function (value) {
        return $.trim(value);
    }
});

jQuery.validator.addClassRules("price", {
    required: true,
    digits: true,
    min: 0,
});

jQuery.validator.addClassRules("stock", {
    required: true,
    digits: true,
    min: 0,
});

$("#createProductForm").validate();
