let html = `<div id="repetitive" class="repetitive">
<div class="mt-5">
    <h2>
        Product Details
    </h2>
</div>
<div class="card-footer p-2"></div>
<div class="mb-3">
    <label class="form-label">Name</label>
    <input type="text" class="form-control product-name" id="nameProduct" name="products[*][name]" placeholder="Name of Product" />
</div>
<div class="mb-3">
    <label class="form-label">Description</label>
    <textarea class="form-control product-description" name="products[*][description]" rows="5" style="resize: none" placeholder="Some short description of product"></textarea>
</div>
<div class="mb-3">
    <div class="row">
        <div class=" col-md-6">
            <label class="form-label">Price</label>
            <input type="number" class="form-control product-price" name="products[*][price]" placeholder="₹0.00" />
        </div>
        <div class=" col-md-6">
            <label class="form-label">Stock</label>
            <input type="number" class="form-control product-stock" name="products[*][stock]" placeholder="0" />
        </div>
    </div>
</div>
<div class="mb-3">
    <div class="form-label">Status</div>
    <select class="form-select product-status" id="status" name="products[*][status]">
        <option value="1">Active</option>
        <option value="2">InActive</option>
    </select>
</div>
</div>`;

class ProductForm {
    static count = 0;
    static getProductForm() {
        let product_form_mod;
        product_form_mod = html.replaceAll('products[*]', 'products['+ ProductForm.count + ']');
        // product_form_mod = product_form_mod.replaceAll('innerProduct-no', 'innerProduct-' + ProductForm.count);
        return product_form_mod;
    }
}

$("#add").click(function() {
    let productHtml = ProductForm.getProductForm();
    ProductForm.count++;
    $("#end").append(productHtml)
    $("#deleteProduct").removeClass("d-none");
});

$("#deleteProduct").click(function () {
let lastProduct = $(".repetitive").last().remove();
ProductForm.count--;
$("#repetitive").length === 0 ? $("#deleteProduct").addClass("d-none") : "";
});


jQuery.validator.addClassRules("name", {
    required: true,
    minlength: 2,
    maxlength: 255,
    normalizer: function (value) {
        return $.trim(value);
    }
});
jQuery.validator.addClassRules("product-name", {
    required: true,
    minlength: 2,
    maxlength: 255,
    normalizer: function (value) {
        return $.trim(value);
    }
});
jQuery.validator.addClassRules("product-price", {
    required: true,
    number: true,
});
jQuery.validator.addClassRules("product-stock", {
    required: true,
    digits: true,
    min: 2,
});
$("#createCategoryForm").validate();
