class MyURL {
    static currentURL;
}

jQuery.validator.addClassRules("name", {
    required: true,
    minlength: 2,
    maxlength: 255
});

jQuery.validator.addClassRules("date", {
    required: true,
});

jQuery.validator.addClassRules("email", {
    required: true,
    email: true,
});

jQuery.validator.addClassRules("file", {
    required: true,
    file: true,
});

$("#importData").validate();

MyURL.currentURL = window.location.toString();
$('#myModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
})
$(document).ready(function () {
    $(".date").flatpickr();
})


$("#searchForm").on("submit", function (evt) {
    evt.preventDefault();
    let current_URL = new URL(window.location);
    let new_URL = new URLSearchParams();
    let name = this.elements["name"].value;
    let email = this.elements["email"].value;
    let orderDate = this.elements["orderDate"].value;
    let product = $(".select2-product")[0].value;
    if (name !== "") {
        table.columns(1)
            .search(name)
        console.log(name);
        new_URL.append("name", name)
    }
    if (email !== "") {
        table
            .columns(2)
            .search(email);
        new_URL.append("email", email)
    }
    if (orderDate !== "") {
        table
            .columns(3)
            .search(orderDate);
        console.log(orderDate);
        new_URL.append("orderDate", orderDate)
    }
    if(product !== "") {
        filterOrders(product);
    }
    table.draw();
    if (name === "" && email === "" && orderDate === "") {
        location.reload();
    }
    let str = (current_URL.toString() + "?" + new_URL.toString());
    MyURL.currentURL = str
    console.log(MyURL.currentURL);
});

