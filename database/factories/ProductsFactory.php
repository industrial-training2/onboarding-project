<?php

namespace Database\Factories;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductsFactory extends Factory
{
    protected $model = Product::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $categoryId = Category::all()->random()->id;
        return [
            "name" => fake()->name,
            "description" => fake()->paragraph(2),
            "price" => random_int(1, 100000),
            "stock" => random_int(0, 1000),
            "is_active" => random_int(0, 1),
            "category_id" => $categoryId,
        ];
    }
}
