<?php

namespace Database\Factories;

use App\Features\Orders\Domain\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Order::class;
    public function definition(): array
    {
        return [
            "customer_name" => $this->faker->name(),
            "customer_email" => $this->faker->unique()->safeEmail(),
            "order_date" => $this->faker->date(),
            "net_total" => $this->faker->numberBetween(1000, 10000),
            "order_status" => "pending",
        ];
    }
}
