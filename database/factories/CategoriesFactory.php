<?php

namespace Database\Factories;

use App\Features\Categories\Domain\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class CategoriesFactory extends Factory
{
    protected $model = Category::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $categories = Category::all()->pluck("id");
        return [
            "name" => fake()->name,
            "description" => fake()->paragraph(2),
            "is_active" => random_int(0, 1),
        ];
    }
}
